<?php
use Illuminate\Support\Facades\Route;
Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/', function () {
    return view('welcome');
});
//New User Registration
Route::get('/register', 'Auth\RegisterController@index');
Route::post('/register', 'Auth\RegisterController@create');
Route::post('/registerProfessional', 'Auth\RegisterController@createProfessional');
Route::get('/rateemotions','EmotionsController@index')->name('rateemotions');
Route::post('/rateemotions','EmotionsController@store')->name('rateemotions.store');
Route::get('/addemotions', 'EmotionsController@create')->name('addemotions');
Route::post('/addemotions', 'EmotionsController@storeEmotion')->name('addemotion.storeEmotion');

Route::get('/addemotionsuser/{value}', 'EmotionsController@userCreate')->name('addemotions.user');
Route::post('/addemotionsuser', 'EmotionsController@userStoreEmotion')->name('addemotion.storeEmotion.user');

Route::get('/trackvalues', 'ValuesController@index')->name('trackvalues');
Route::post('/trackvalues', 'ValuesController@store')->name('trackvalues.store');
Route::get('/addvalues', 'ValuesController@create')->name('addvalues');
Route::post('/addvalues', 'ValuesController@storeValue')->name('addvalues.store');

Route::get('/terms', 'HomeController@terms')->name('terms');

Route::group(['middleware' => ['userLoggedIn', 'detailsComplete']], function(){
//Users Control Panel
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::get('/reminders', 'RemindersController@index')->name('reminders.index');
Route::post('/reminders', 'RemindersController@update')->name('reminders.update');
Route::get('/feedback', 'HomeController@feedback')->name('feedback');
Route::get('/feedback/sendmail', 'HomeController@sendfeedback')->name('feedback.send');
Route::put('/updateProfile/{id}', 'ProfileController@update')->name('profile.update');
Route::put('/updateHealthcareProvider/{id}', 'ProfileController@updateHealthcareProvider')->name('healthcare.update');
//tracking emotions
Route::get('/track', 'SessionsController@storeSession')->name('trackemotions');
Route::get('/track/{id}', 'TrackEmotions@show')->name('trackemotions');
Route::get('/changeemotions/{id}/value/{valueId}','EmotionsController@change')->name('changeemotions');
Route::post('/changeemotions','EmotionsController@update')->name('changeemotions.update');
Route::post('/track', 'SessionsController@store')->name('trackemotions.store');
Route::get('/addnotes', 'SessionsController@createNotes')->name('createNotes');
Route::post('/addnotes', 'SessionsController@storeNotes')->name('storeNotes');

//viewing results
Route::get('/overallsummary', 'SummaryController@index')->name('overallsummary');
Route::patch('/overallsummary/update/', 'SummaryController@order')->name('overallsummary.update');
Route::get('/summarydetails/{id}', 'SummaryController@show')->name('summarydetails');
Route::get('/compare', 'SummaryController@compare')->name('compare');
Route::post('/compareresults', 'SummaryController@compareresults')->name('compareresults');
Route::get('/chart', 'SummaryController@charts')->name('charts');
Route::get('/getChartData', 'SummaryController@getChartData')->name('charts.get');
Route::get('/diary', 'SummaryController@diary')->name('diary');

//resources
Route::get('/resources/{id}', 'ResourcesController@show')->name('resources');
Route::post('/update_resource_click', 'ResourcesController@updateClickCount');
});

Route::group(['middleware' => ['userLoggedIn', 'isAdmin']], function(){
//Admin Routes
Route::get('/admin', 'AdminController@index')->name('admin');
//Admin Resources routes
Route::get('/admin/resources', 'AdminResourcesController@index')->name('admin.resources');
Route::post('/admin/resources', 'AdminResourcesController@store')->name('admin.resources.store');
Route::put('/admin/resources/{id}', 'AdminResourcesController@update')->name('admin.resources.update');
Route::post('/admin/resources/{id}', 'AdminResourcesController@destroy')->name('admin.resources.delete');
//Admin Resources Category routes
Route::get('/admin/resourceCategories', 'adminResourceCategoriesController@index')->name('admin.resourceCategories');
Route::post('/admin/resourceCategories', 'adminResourceCategoriesController@store')->name('admin.resourceCategories.store');
Route::put('/admin/resourceCategories/{id}', 'adminResourceCategoriesController@update')->name('admin.resourceCategories.update');
Route::post('/admin/resourceCategories/{id}', 'adminResourceCategoriesController@destroy')->name('admin.resourceCategories.delete');
//Admin Company routes
Route::get('/admin/companies', 'adminCompaniesController@index')->name('admin.companies');
Route::post('/admin/companies', 'AdminProfessionalsController@store')->name('admin.companies.store');
Route::put('/admin/companies/{id}', 'adminCompaniesController@update')->name('admin.companies.update');
Route::post('/admin/companies/{id}', 'adminCompaniesController@destroy')->name('admin.companies.delete');
//Admin Professionals Routes
Route::get('/admin/professionals/', 'AdminProfessionalsController@index')->name('admin.professionals');
Route::post('/admin/professionals', 'AdminProfessionalsController@store')->name('admin.professionals.store');
Route::put('/admin/professionals/{id}', 'AdminProfessionalsController@update')->name('admin.professionals.update');
Route::post('/admin/professionals/{id}', 'AdminProfessionalsController@destroy')->name('admin.professionals.delete');

Route::get('/admin/emotions', 'AdminEmotionsController@index')->name('admin.emotions');
Route::post('/admin/emotions', 'AdminEmotionsController@store')->name('admin.emotions.store');
Route::put('/admin/emotions/{id}', 'AdminEmotionsController@update')->name('admin.emotions.update');
Route::post('/admin/emotions/{id}', 'AdminEmotionsController@destroy')->name('admin.emotions.delete');

Route::get('/admin/conditions', 'AdminConditionsController@index')->name('admin.conditions');
Route::get('/admin/conditions/{id}', 'adminConditionsController@show')->name('admin.conditions.show');
Route::put('/admin/conditions/{id}', 'adminConditionsController@update')->name('admin.conditions.update');

Route::get('/admin/users/', 'AdminUsersController@index')->name('admin.users');
Route::put('/admin/users/{id}', 'AdminUsersController@update')->name('admin.users.update');
Route::post('/admin/users/{id}', 'AdminUsersController@destroy')->name('admin.users.delete');


});
Route::group(['middleware' => ['userLoggedIn', 'isProfessional']], function(){
//Admin Routes for healthcare professional
Route::get('/dashboard', 'AdminController@professionalIndex')->name('dashboard');
Route::get('/dashboard/patients', 'dashboardPatientsController@index')->name('dashboard.patients.index');
Route::get('/dashboard/patients/{id}', 'adminConditionsController@show')->name('dashboard.patients.show');
Route::post('/dashboard/patients/', 'adminConditionsController@store')->name('dashboard.patients.store');
Route::put('/dashboard/patients/{id}', 'adminConditionsController@update')->name('dashboard.patients.update');
Route::get('/dashboard/patients/session/{session}', 'adminConditionsController@sessions')->name('dashboard.sessions.show');
Route::get('/dashboard/patients/scores/{id}', 'adminConditionsController@showScores')->name('dashboard.scores.show');
Route::get('/dashboard/admin', 'AdminController@companyAdmin')->name('dashboard.admin');
Route::post('/dashboard/admin', 'AdminProfessionalsController@store')->name('dashboard.admin.store');
Route::put('/dashboard/admin/{id}', 'AdminProfessionalsController@updateProf')->name('dashboard.admin.update');
Route::post('/dashboard/admin/{id}', 'AdminProfessionalsController@destroy')->name('dashboard.admin.destroy');
});

Route::get('/auth/passwordset/{token}', 'PasswordSetupController@passwordset');