<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionValues extends Model
{
    protected $fillable = [
        'order'
    ];

    public function value()
    {
        return $this->belongsTo(Value::class);
    }

    public function valueName()
    {
        return $this->belongsTo(Value::class, 'value_id')->get()->pluck('name')->first();
    }

    public function emotions() 
    {
        return $this->hasMany(EmotionSessionValue::class, 'session_value_id');
    }
}
