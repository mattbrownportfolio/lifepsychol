<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emotion extends Model
{
    public function user()
    {
        return $this->belongsToMany(User::class)->withPivot('emotion_score');
    }

    public function filter()
    {
        return $this->belongsTo(EmotionFilter::class);
    }
}
