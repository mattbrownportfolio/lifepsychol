<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Value extends Model
{
    public function user()
    {
        return $this->belongsToMany(User::class);
    }
    public function sessionvalues()
    {
        return $this->belongsTo(SessionValues::class, "value_id");
    }
    
}
