<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Professional extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'code', 'company_id', 'maximum_number_of_codes', 'used'
    ];
    
    public function user()
    {
        return $this->hasOne(User::class)->withTrashed();
    }

    public function company()
    {
        return $this->belongsTo(Company::class)->withTrashed();
    }

    public function CompanyUser()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
