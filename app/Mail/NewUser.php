<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $conditions;

    public function __construct($conditions)
    {
        $this->conditions = $conditions;
        $this->url = 'www.google.com';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.newuser')
                    ->with([
                        'conditions' => $this->conditions,
                        'url' => $this->url
                    ]);
    }
}
