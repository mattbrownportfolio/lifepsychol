<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendHCProviderChangeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $code;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $code)
    {
        $this->name = $name;
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.HCProviderChange')
                    ->subject('Changes to your healthcare provider')
                    ->with([
                        'name' => $this->name,
                        'code' => $this->code
                    ]);
    }
}
