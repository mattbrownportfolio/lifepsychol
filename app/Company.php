<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'short_name', 'maximum_code_count', 'used', 'contactname', 'contactemail'
    ];
    
    public function professional()
    {
        return $this->hasOne(Professional::class);
    }
}
