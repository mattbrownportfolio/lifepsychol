<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function tags()
    {
        return $this->hasMany(Tag::class);
    }
}
