<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmotionFilter extends Model
{
    public function emotion()
    {
        return $this->belongsTo(Emotion::class);
    }
}
