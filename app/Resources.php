<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resources extends Model
{
    use SoftDeletes;

    public function category()
    {
        return $this->belongsTo(ResourceCategories::class);
    }

    protected $fillable = [
        'category_id',
        'link_url',
        'link_text',
        'description'
    ];

}
