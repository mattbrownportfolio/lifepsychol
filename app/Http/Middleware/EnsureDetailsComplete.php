<?php

namespace App\Http\Middleware;

use Closure;
use App\EmotionUser;
use App\User;
use Auth;

class EnsureDetailsComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userEmotions = EmotionUser::where('user_id', Auth::user()->id)->get();
        $userValues = User::find(Auth::user()->id)->values()->get();

        if($userEmotions->count() == 0) {
            return redirect('rateemotions');
        } else if($userValues->count() == 0) {
            return redirect('trackvalues');
        }     
        return $next($request);
    }
}
