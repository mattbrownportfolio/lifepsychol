<?php

namespace App\Http\Controllers;

use App\Value;
use App\Emotion;
use App\User;
use Auth;
use Illuminate\Http\Request;

class ValuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $allValues = Value::where('addedby', null)->orWhere('addedby', Auth::user()->id)->get()->sortBy('name');
        return view('newuser.trackvalues', compact('allValues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allValues = Value::all()->sortBy('name');
        return view('newuser.addvalues', compact('allValues'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $scoresArray = [];
        $user = User::find(Auth::user()->id);
        foreach($request->value as $key=>$selected) {   
            array_push($scoresArray, $key);
        }        
        if($user->values()->exists()) {                
            $user->values()->sync($scoresArray); 
        } else {
            $user->values()->attach($scoresArray);
        }
        
        return redirect()->route('home');
    }

    public function storeValue(Request $request)
    {
        if(Emotion::where('name', $request->value)->exists()) {
            return redirect()->back()->with('error', 'Sorry, this emotion already exists.');
        } else {
            $value = new Value;
            $value->name = $request->value;
            $value->addedBy = Auth::user()->id;
            $value->save();            
            return redirect()->route('trackvalues', ['id' => $request->valueId])->with('success', 'Value successfully added.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Value  $value
     * @return \Illuminate\Http\Response
     */
    public function show(Value $value)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Value  $value
     * @return \Illuminate\Http\Response
     */
    public function edit(Value $value)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Value  $value
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Value $value)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Value  $value
     * @return \Illuminate\Http\Response
     */
    public function destroy(Value $value)
    {
        //
    }
}
