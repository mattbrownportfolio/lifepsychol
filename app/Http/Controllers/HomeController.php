<?php

namespace App\Http\Controllers;

use App\Mail\Feedback;
use Illuminate\Http\Request;
use App\Sessions;
use Auth;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lastSession = Sessions::where('user_id', Auth::user()->id)->get();     
        return view('home.home', compact('lastSession'));
    }

    public function feedback()
    {
        return view('home.feedback');
    }
    public function sendfeedback(Request $request)
    {
        Mail::to('something@else.com')->send(new Feedback($request->feedback));
        return redirect()->route('home')->with('success', 'Feedback Sent!');
    }

    public function terms()
    { 
        return view('newuser.terms');
    }
}
