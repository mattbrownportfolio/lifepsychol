<?php

namespace App\Http\Controllers;

use App\Emotion;
use App\EmotionFilter;
use App\EmotionUser;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmotionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $user = User::where('id', Auth::user()->id)->get()->first();
        // if($user->gender == '1' || $user->gender == '2') {
        //     $filteredEmotions = EmotionFilter::where('gender', $user->gender)->where('trauma', $user->trauma)->orderBy('emotion_id')->get();
        //     $filteredEmotionsId = EmotionFilter::where('gender', $user->gender)->where('trauma', $user->trauma)->pluck('emotion_id');
        //     $otherEmotions = Emotion::whereNotIn('id', $filteredEmotionsId)->where('addedBy', null)->orWhere('addedBy', Auth::user()->id)->orderBy('name')->get();
        // } else {
        //     $filteredEmotions = Emotion::where('addedBy', null)->orderBy('name')->get();
        //     $otherEmotions = null;
        // }
        
        $filteredEmotions = EmotionFilter::all()->sortBy('emotion.name');
        $filteredEmotionsId = $filteredEmotions->pluck('emotion_id');
        $otherEmotions = Emotion::whereNotIn('id', $filteredEmotionsId)->where('addedBy', null)->orWhere('addedBy', Auth::user()->id)->orderBy('name')->get();
        return view('newuser.rateemotions', compact('filteredEmotions', 'otherEmotions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allEmotions = Emotion::all();
        return view('newuser.addemotions', compact('allEmotions'));
    }

    public function userCreate()
    {
        $allEmotions = Emotion::all();
        return view('newuser.adduseremotions', compact('allEmotions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $existingUserEmotions = EmotionUser::where('user_id', Auth::user()->id)->get();       
        if($existingUserEmotions->isempty()) {
            foreach($request->emotions as $key=>$score) {
                $user = User::find(Auth::user()->id);
                $user->emotions()->attach($key, ['emotion_score' => $score, 'archived' => 0]);
            }
        } else {          
            foreach($request->emotions as $key=>$score) {               
                $user = User::find(Auth::user()->id);
                $user->emotions()->sync($key, ['emotion_score' => $score, 'archived' => 0]);
            }
        }        
        return redirect()->route('trackvalues');
    }

    public function storeEmotion(Request $request)
    {
        if(Emotion::where('name', $request->emotion)->exists()) {
            return redirect()->back()->with('error', 'Sorry, this emotion already exists.');
        } else {
            $emotion = new Emotion;
            $emotion->name = $request->emotion;
            $emotion->addedBy = Auth::user()->id;
            $emotion->save();            
            return redirect()->route('rateemotions', ['id' => $request->valueId])->with('success', 'Emotion successfully added.');
        }

    }

    public function userStoreEmotion(Request $request)
    {        
        if(Emotion::where('name', $request->emotion)->exists()) {
            return redirect()->back()->with('error', 'Sorry, this emotion already exists.');
        } else {
            $emotion = new Emotion;
            $emotion->name = $request->emotion;
            $emotion->addedBy = Auth::user()->id;
            $emotion->save();
            return redirect()->route('trackemotions', ['id' => $request->valueId])->with('success', 'Emotion successfully added.');
        }

    }

    public function change(Request $request, $id) {
        $value = $request->valueId;
        session()->put(['value' => $value]);
        $allEmotions = Emotion::whereNull('addedBy')->orWhere('addedBy', Auth::user()->id)->orderBy('name')->get();
        $scoredEmotions = User::find($id)->emotions()->pluck('emotion_score', 'emotion_id')->toArray();
        $scoredEmotionsIds= User::find($id)->emotions()->pluck('emotion_id')->toArray();
        $archivedEmotions = User::find($id)->emotions()->where('archived', "1")->pluck('archived', 'emotion_id')->toArray();
        $userId = $id;
        return view('home.changeEmotions', compact('userId', 'allEmotions', 'scoredEmotions', 'scoredEmotionsIds', 'archivedEmotions', 'value'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Emotion  $emotion
     * @return \Illuminate\Http\Response
     */
    public function show(Emotion $emotion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Emotion  $emotion
     * @return \Illuminate\Http\Response
     */
    public function edit(Emotion $emotion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Emotion  $emotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        foreach($request->emotions as $key=>$score) {
            try {
                if (!User::find(Auth::user()->id)->emotions()->where('emotion_id', $key)->exists()) {
                    $user = User::find(Auth::user()->id);
                    $user->emotions()->attach($key, ['emotion_score' => $score]);
                } else {
                    $user = User::find(Auth::user()->id);
                    $user->emotions()->updateExistingPivot($key, ['emotion_score' => $score]);
                }
            } catch (\Exception $e) {
                dd($e);
            }
        }

        foreach($request->archived as $key=>$archived) {
            $user = User::find(Auth::user()->id);
            $user->emotions()->updateExistingPivot($key, ['archived' => $archived]);
        }
        return redirect()->route('trackemotions', session()->get('value'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Emotion  $emotion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Emotion $emotion)
    {
        //
    }
}
