<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Condition;
use App\User;
use App\Tag;
use App\Professional;
use App\Sessions;
use App\SessionValues;
use App\Company;
use Carbon\Carbon;

use Auth;
class adminConditionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allConditions = Condition::with('users')->get();     
        return view('admin.conditions.index', compact('allConditions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $path = Auth::user()->role_id == 1 ? 'admin.conditions.update' : 'dashboard.patients.update';
        $user = User::find($id);
        $userConditions = $user->condition;        
        $userConditionsArray = array();
        $company = Company::where('contactemail', Auth::user()->email)->get()->first();
        $professional = Professional::where('user_id', Auth::user()->id)->get()->first();

        foreach($userConditions as $userCondition) {
            $tags = Tag::where('added_by', Auth::user()->id)->where('condition_id', $userCondition->id)->get()->pluck('tag');           
            $condition = [
                'condition_id' => $userCondition->id,
                'condition' => $userCondition->condition,
                'tags' => $tags
            ];
            array_push($userConditionsArray, $condition);
        }
        return view('admin.conditions.show', compact('professional', 'userConditionsArray', 'user', 'path', 'company'));
    }

    private function sumArrays(array $a = [], array $b = []) {
		foreach ($a as $k => $v) {//Iterate along the '$a' array, getting key value pair for every array position
			if(array_key_exists($k, $b)){//Check if '$a' key exist in '$b'
			//If key exist into '$b' array
				if(is_array($v)){//Check if value of key is an array
				//If value is an array
					$b[$k] = sumArrays($b[$k],$v);//The function is called itself recursively
				}else{
        //Otherwise
        $b[$k]+=$v;//Accumulates the '$a' value in '$b' key position
        }
        }else{
            //Otherwise
            $b[$k]=$v; //Create key node in '$b'
        }
        }
        //Return merged '$b'
        return $b;
    }

    public function sessions($id) {
        $user = User::find($id);
        $professional = Professional::where('user_id', Auth::user()->id)->get()->first(); 

        $allUserSessions = Sessions::where('user_id', $user->id)->where('complete', 1)->get();   
        $allSessions = $allUserSessions->pluck('id');
        $allValues = SessionValues::whereIn('session_id', $allSessions)->get()->groupBy('value_id');

        $company = Company::where('contactemail', Auth::user()->email)->get()->first();
     
        session(['user' => $user]);

        $dataset = [];
        $dates = [];
        $notes = [];
        $overallScore=[];
        $colours = [
            '#ec1c24',
            '#f6921e',
            '#ffcf40',
            '#d6de23',
            '#8bc53f',
            '#00a69c',
            '#25a9e0',
            '#2b388f',
            '#90278e',            
        ];
        if($allValues->count() > 0){
            
            foreach($allValues->first() as $value){                   
                array_push($dates, Carbon::parse($value['created_at'])->format('d-m-Y'));
            }           
 
        array_push($dataset, [  
            'label' => 'Overall',
            'fill' => false,
            'borderColor' => $colours[0],
            'borderWidth' => 5,
            'lineTension' => 0.2]);
        $overallScore = array_fill(0, count($allValues->first()->pluck('score')), 0);
        $i = 1;
        foreach ($allValues as $value) {         
            $overallScore = $this->sumArrays($overallScore, $value->pluck('score')->toArray());
            array_push($dataset, [
                'data' =>  $value->pluck('score'),
                'label' => $value[0]->valueName(),
                'fill' => false,
                'borderColor' => $colours[$i],
                'borderWidth' => 5,
                'lineTension' => 0.2,
                'id' => $value->pluck('id')
                ]);
            $i++;
        }
        $dataset[0]['data'] = $overallScore;
        $data = ['allDates' => $dates, 'datasets' => $dataset];
    } else {
        $data = 0;
    }
        
        return view('admin.scores.showAllSessions', compact('professional', 'allUserSessions', 'user', 'data', 'company'));
    }

    public function showScores($id) {
        $user = User::find($id);
        $allScores = SessionValues::where('session_id', $id)->get();
        $professional = Professional::where('user_id', Auth::user()->id)->get()->first();   
        $company = Company::where('contactemail', Auth::user()->email)->get()->first();          
        return view('admin.scores.showAllScores', compact('professional', 'allScores', 'user', 'company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tags = explode(',', $request->tags); 
        try {
            if($request->tags != null) {
                foreach($tags as $key => $tag) {
                    $newTag = new Tag;
                    $newTag->tag = $tag;
                    $newTag->condition_id = $request->condition_id;
                    $newTag->user_id = $request->user_id;
                    $newTag->added_by = Auth::user()->id;
                    $newTag->save();            
                }
            }            
        } catch (\Exception $e) {
            dd($e);
        } 
        return redirect()->back()->with('status', 'Tags Added');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $newTags = explode(',', $request->tags);
        $existingTags = Tag::where('condition_id', $request->condition_id)->get()->pluck('id')->toArray();              
        Tag::destroy($existingTags);
        try {
            foreach($newTags as $key => $tag) {
                $newTag = new Tag;
                $newTag->tag = $tag;
                $newTag->condition_id = $request->condition_id;
                $newTag->user_id = $request->user_id;
                $newTag->added_by = Auth::user()->id;
                $newTag->save();            
            }
        } catch (\Exception $e) {
            dd($e);
        } 
        return redirect()->back()->with('status', 'Tags Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
