<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sessions;
use Session;
use App\SessionValues;
use App\Value;
use App\User;
use App\EmotionSessionValue;
use App\UserEmotionScore;
use Auth;

class SessionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeSession(Request $request)
    {
        $sessionkey = rand(11111111,99999999);
        session()->put('key', $sessionkey);
        $session = new Sessions();
        $session->user_id = Auth::user()->id;
        $session->sessionKey = $sessionkey;
        $session->complete = 0;
        $session->stage = 1;
        $session->save();
        return redirect()->route('trackemotions', 1);
    }

    public function store(Request $request)
    {
        $sessionId = Sessions::where('sessionKey', session()->get('key'))->pluck('id')->first();
        $score = 0;
        $emotions = User::find(Auth::user()->id)->emotions()->whereIn('emotion_id', array_keys($request->emotions))->get();
        foreach($emotions as $emotion) {
            $emotionScore = $emotion->pivot->emotion_score;
            $score += $emotionScore;
        }
        $score /= count($emotions);

        $existingSessionValue = SessionValues::where('value_id', $request->valueId)->where('session_id', $sessionId)->get();    
        
        $sessionValues = new SessionValues;
        $sessionValues->session_id = $sessionId;
        $sessionValues->value_id = $request->valueId;
        $sessionValues->score = number_format($score, 2, '.', '');
        $sessionValues->save();
        $sessionValueId = $sessionValues->id;        

        foreach($request->emotions as $key=>$id) {
            $emotionValues = new EmotionSessionValue;
            $emotionValues->session_value_id = $sessionValues->id;
            $emotionValues->user_emotion_score_id = $key;
            $emotionValues->save();            
        }

        if(!$existingSessionValue->isEmpty()) {
            $existingSessionValue->first()->delete();   
            foreach($existingSessionValue as $emotionvalue) {
                $exitingEmotionvalue = EmotionSessionValue::find($emotionvalue->id);
                $exitingEmotionvalue->delete();
            }
        } 

        $pageNumber = $request->pageNumber;

        $currentSession = Sessions::where('sessionKey', session()->get('key'))->first();
        $currentSession->stage = $pageNumber;
        $currentSession->save();

        
        session()->put(['pageNumber' => $pageNumber, 'sessionValueId' => $sessionValueId]);
        return redirect()->route('createNotes');
    }

    public function createNotes()
    {
        $index = session()->get('pageNumber');
        $session_value_id = session()->get('sessionValueId');

        $valueById = User::find(Auth::user()->id)->values()->get()->pluck('id');
        $allValuesCount = count($valueById);
        if(!is_numeric($index) || ($index <= 0 ) || ($index > $allValuesCount)) {
            return redirect()->route('home');
        }
        $isLast = false;
        if($allValuesCount <= $index) {
            $sessionId = Sessions::where('sessionKey', session()->get('key'))->pluck('id')->first();
            $session = Sessions::find($sessionId);
            $session->complete = 1;
            $session->save();
            $isLast = true;
        }
        $allValuesCount = count($valueById);
        $id = $valueById[$index - 1];
        $allValuesNameId = SessionValues::where('id', $session_value_id)->get()->pluck('value_id')->first();
        $allValuesName = Value::where('id', $allValuesNameId)->get()->pluck('name')->first();
        $allValuesId = Value::whereIn('id', $valueById)->where('id', $id)->get()->pluck('id')->first();
        return view('home.addnotes', compact('allValuesCount', 'index', 'allValuesName', 'allValuesId', 'session_value_id', 'isLast'));
    }

    public function storeNotes(Request $request)
    {
        $valueById = User::find(Auth::user()->id)->values()->get()->pluck('id');
        $allValuesCount = count($valueById);
        $id = SessionValues::where('id', $request->sessionValueId)->pluck('id')->first();
        $sessionValue = SessionValues::findOrFail($id);
        $sessionValue->note = $request->note;
        $sessionValue->save();
        if($request->pageNumber >= $allValuesCount) {
            return redirect()->route('overallsummary');
        } else {
            return redirect()->route('trackemotions', (int)$request->pageNumber + 1 );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
