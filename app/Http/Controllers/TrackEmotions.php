<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emotion;
use App\Value;
use App\User;
use App\UserEmotionScore;
use Auth;
class TrackEmotions extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($index)
    {
        $emotions = User::find(Auth::user()->id)->emotions()->where('archived', '!=', '1')->get();
        $valueById =  User::find(Auth::user()->id)->values()->get()->pluck('id');
        $allValuesCount = count($valueById);
        if(!is_numeric($index) || ($index <= 0 ) || ($index > $allValuesCount)) {
            return redirect()->route('home');
        }
        $id = $valueById[$index - 1];
        $allValuesName = Value::whereIn('id', $valueById)->where('id', $id)->get()->pluck('name')->first();
        $allValuesId = Value::whereIn('id', $valueById)->where('id', $id)->get()->pluck('id')->first();
        // session()->put(['valuesName' => $allValuesName, 'returnPageNumber' => $index]);
        return view('home.track', compact('id','allValuesName', 'allValuesId', 'allValuesCount', 'index', 'emotions', 'valueById'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
