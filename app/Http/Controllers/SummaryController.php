<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Sessions;
use App\SessionValues;
use App\EmotionUser;
use App\EmotionSessionValue;
use App\Value;
use App\Emotion;
use Carbon\Carbon;


class SummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sessionId = Sessions::where('sessionKey', session()->get('key'))->pluck('id')->first();

        $sessionValues = SessionValues::where('session_id', $sessionId)->orderBy('order')->get();

        foreach($sessionValues as $value){
            $value['emotionUsageCount'] = $value->emotions->count();           
        }
        $sessionValues = $sessionValues->sortByDesc('emotionUsageCount');
        $allSessionScores = SessionValues::where('session_id', $sessionId)->get()->pluck('score')->toArray();
        $sessionScore = array_sum($allSessionScores);
        $sessionCount = Sessions::where('user_id', Auth::user()->id)->get()->count();
        return view('home.overallsummary', compact('sessionValues', 'sessionScore', 'sessionCount'));
    }

    public function compare()
    {
        $allSessions = Sessions::where('user_id', Auth::user()->id)->where('complete', 1)->get();
        $sessionId = Sessions::where('sessionKey', session()->get('key'))->pluck('id')->first();
        $allSessionScores = SessionValues::where('session_id', $sessionId)->get()->pluck('score')->toArray();
        $sessionScore = array_sum($allSessionScores);
        return view('home.compare', compact('allSessions', 'sessionId', 'sessionScore'));
    }

    public function compareResults(Request $request)
    {
        $sessionDate = Sessions::where('id',  $request->compareDates)->get()->pluck('created_at')->first();
        $oldDate = Carbon::parse($sessionDate)->format('d-m-Y');
        $currentSessionValues = SessionValues::where('session_id',  $request->currentSessionId)->orderBy('order')->get();
        $oldSessionValues = SessionValues::where('session_id',  $request->compareDates)->orderBy('order')->get();
        return view('home.compareresults', compact('currentSessionValues', 'oldSessionValues', 'oldDate'));
    }

    private function sumArrays(array $a = [], array $b = []) {
		foreach ($a as $k => $v) {//Iterate along the '$a' array, getting key value pair for every array position
			if(array_key_exists($k, $b)){//Check if '$a' key exist in '$b'
			//If key exist into '$b' array
				if(is_array($v)){//Check if value of key is an array
				//If value is an array
					$b[$k] = sumArrays($b[$k],$v);//The function is called itself recursively
				}else{
        //Otherwise
        $b[$k]+=$v;//Accumulates the '$a' value in '$b' key position
        }
        }else{
            //Otherwise
            $b[$k]=$v; //Create key node in '$b'
        }
        }
        //Return merged '$b'
        return $b;
    }
    public function charts()
    {
        $allSessions = Sessions::where('user_id', Auth::user()->id)->where('complete', 1)->get()->pluck('id');
        $allValues = SessionValues::whereIn('session_id', $allSessions)->get()->groupBy('value_id');
    
        $dataset = [];
        $dates = [];
        $notes = [];
        $overallScore=[];
        $colours = [
            '#ec1c24',
            '#f6921e',
            '#ffcf40',
            '#d6de23',
            '#8bc53f',
            '#00a69c',
            '#25a9e0',
            '#2b388f',
            '#90278e',            
        ];
        if($allValues->count() > 0){
            foreach($allValues->first() as $value){
                array_push($dates, Carbon::parse($value['created_at'])->format('d-m-Y'));
            }           
        
        array_push($dataset, [  
            'label' => 'Overall',
            'fill' => false,
            'borderColor' => $colours[0],
            'borderWidth' => 5,
            'lineTension' => 0.2]);
        $overallScore = array_fill(0, count($allValues->first()->pluck('score')), 0);
        $i = 1;
        foreach ($allValues as $value) {         
            $overallScore = $this->sumArrays($overallScore, $value->pluck('score')->toArray());
            array_push($dataset, [
                'data' =>  $value->pluck('score'),
                'label' => $value[0]->valueName(),
                'fill' => false,
                'borderColor' => $colours[$i],
                'borderWidth' => 5,
                'lineTension' => 0.2,
                'id' => $value->pluck('id')
                ]);
            $i++;
        }
        $dataset[0]['data'] = $overallScore;
        $data = ['allDates' => $dates, 'datasets' => $dataset];
    } else {
        $data = 0;
    }
        return view('home.chart', compact('data'));
    }

    public function getChartData()
    {

        $valueInformation = SessionValues::where('id', request('id'))->first();
    
        return response()->json([
            'note' => $valueInformation->note ?? 'No note added',
            'score' => $valueInformation->score,
            'date' => Carbon::parse($valueInformation->created_at)->format('d/m/Y'),
            'valueName' => $valueInformation->valueName()
        ]);
    }

    public function diary()
    {
        $sessions = Sessions::where('user_id', Auth::user()->id)->get()->pluck('id');
        $allSessionValues = SessionValues::whereIn('session_id', $sessions)->whereNotNull('note')->get();
        $events = [];
        foreach($allSessionValues as $sessionValue) {
            array_push($events, [
                'title' => $sessionValue->valueName(),
                'start' => Carbon::parse($sessionValue->created_at)->format('Y-m-d'),
                'note' => $sessionValue->note                
            ]);
        }
        $events = ['events' => $events];
        return view('home.diary', compact('events'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $valueId = SessionValues::where('id', $id)->get()->pluck('value_id')->first();
        $valueScore = SessionValues::where('id', $id)->get()->pluck('score')->first();
        $valueName = Value::where('id', $valueId)->get()->pluck('name')->first();

        $emotionSessionIds = EmotionSessionValue::where('session_value_id', $id)->get()->pluck('user_emotion_score_id');

        $emotions =  Emotion::whereIn('id', $emotionSessionIds)->get();
        $note = SessionValues::where('id', $id)->get()->pluck('note')->first();
        return view('home.summarydetails', compact('valueName', 'emotions', 'note', 'valueScore'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function order()
    {
        if (request()->has('value')) {
            foreach(request('value') as $order => $value_id) {               
                $value = SessionValues::findOrFail($value_id);                
                $value->update([
                    'order' => $order
                ]);
            }
        }
  }
}
