<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    public function redirectPath()
    {
        if (Auth::user()->role_id == 1) {
            return route('admin');
        } else if (Auth::user()->role_id == 2) {
            return route('home');
        } else if (Auth::user()->role_id == 3) {
            return route('dashboard');
        } 

       
    }
    // protected $redirectTo = RouteServiceProvider::HOME;
}
