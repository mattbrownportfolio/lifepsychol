<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmotionUser extends Model
{
    protected $table = 'emotion_user';
}
