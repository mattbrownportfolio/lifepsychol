@extends('layouts.newuser', [
    'title' => 'Rate Emotions'
])
@section('content')
    <div class="container">
        <div class="row mb-2">
            <div class="col-12 text-center">
                <h1>Emotion Word Selection</h1>
                <h3>Step 2</h3>
            </div>
        </div>
    <div class="px-4">
        <div class="userInstruction">
            To help us personalise the {{config('app.name')}} tracker, please place the following 18 words into one of  three categories by clicking the relevant box. Please note these words are pre-selected based on research undertaken during the development of the Tracker. If there is an emotion word you would prefer to use, please click ‘Emotions Bank’ to access other words available.
        </div>       
        <h2 class="text-center pt-2 emotionCounterWording"><span class="emotionCounter">0</span>/18 Selected</h2>
        <div class="row pt-3 stickyHeaderEmotion">
            <div class="col-2 px-2 offset-6 text-center d-none d-md-block">
                Positive
            </div>
            <div class="col-2 px-2 offset-6 text-center d-block d-md-none">
                +ve
            </div>
            <div class="col-2 px-2 text-center">
                Neutral
            </div>
            <div class="col-2 px-2 text-center d-none d-md-block">
                Negative
            </div>            
            <div class="col-2 pr-2 text-center d-block d-md-none">
                -ve
            </div>
        </div>        
        <form action="{{ route('rateemotions.store') }}" method="POST" id="emotionsForm">
            @csrf
            @foreach( $filteredEmotions as $emotions )
                <div class="row pt-2">
                    <div class="col-6 registerText">{{ $otherEmotions === null ? $emotions->name : $emotions->emotion->name }}</div>
                    <div class="col-2 text-center">
                        <input type="checkbox" id="emotionCheckbox" name="emotions[{{ $emotions->id }}]" class="registerInputRadio radio" value='1'>
                    </div>
                    <div class="col-2 text-center">
                        <input type="checkbox" id="emotionCheckbox" name="emotions[{{ $emotions->id }}]" class="registerInputRadio radio" value='0'>
                    </div>
                    <div class="col-2 text-center">
                        <input type="checkbox" id="emotionCheckbox" name="emotions[{{ $emotions->id }}]" class="registerInputRadio radio" value='-1'>
                    </div>
                </div>
            @endforeach
            @if($otherEmotions === null)
                <p class="pt-4" style="font-size: 15px">Not seeing your emotion? <a href="{{ url('/addemotions') }}">Click here</a></p>
    
            @elseif($otherEmotions != null)
            <div class="row text-center my-4">
                <div class="col-12">
                    <div class="userInstruction">
                        If you feel the above emotion words are not totally suitable for you, please click on the below button to see a larger list of emotions that may be more suitable. <br>
                        <div class="btn btn-primary btn-lg mt-3" id="showAll">Emotions Bank</div>
                    </div>
                </div>
            </div>
            <div id="other" style="display: none;">
                <h2 class="text-center pt-2 emotionCounterWording"><span class="emotionCounter">0</span>/18 Selected</h2>
                <div class="row pt-3">
                    <div class="col-2 px-2 offset-6 text-center d-none d-md-block">
                        Positive
                    </div>
                    <div class="col-2 px-2 offset-6 text-center d-block d-md-none">
                        +ve
                    </div>
                    <div class="col-2 px-2 text-center">
                        Neutral
                    </div>
                    <div class="col-2 px-2 text-center d-none d-md-block">
                        Negative
                    </div>            
                    <div class="col-2 pr-2 text-center d-block d-md-none">
                        -ve
                    </div>
                </div> 
               @foreach( $otherEmotions as $otherEmotion )
                   <div class="row pt-2">
                       <div class="col-6 registerText">{{ $otherEmotion->name }}</div>
                       <div class="col-2 text-center">
                           <input type="checkbox" id="emotionCheckbox" name="emotions[{{ $otherEmotion->id }}]" class="registerInputRadio radio" value='1'>
                       </div>
                       <div class="col-2 text-center">
                           <input type="checkbox" id="emotionCheckbox" name="emotions[{{ $otherEmotion->id }}]" class="registerInputRadio radio" value='0'>
                       </div>
                       <div class="col-2 text-center">
                           <input type="checkbox" id="emotionCheckbox" name="emotions[{{ $otherEmotion->id }}]" class="registerInputRadio radio" value='-1'>
                       </div>
                   </div>
               @endforeach
                <p class="pt-4" style="font-size: 15px">Not seeing your emotion? <a href="{{ url('/addemotions') }}">Click here</a></p>
            </div>
            @endif
            <h2 class="text-center pt-2 emotionCounterWording"><span class="emotionCounter">0</span>/18 Selected</h2>
            <div class="row pt-4">
                <div class="col-12 form-group">
                    <button href="#" class="btn btn-primary btn-block loginButton" type="submit">Next</button>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Scoring Emotions</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                If you wish to select a different word (emotion) from the 18 that have been pre-selected for you, please click on the "EMOTIONS BANK" button. You will now see approximately 60 words (emotions) to choose from. Please rate the 18 of your choice. If you do not select 18, you will be prompted to increase or decrease your selections until you have chosen 18 in total.
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="emotionsModal" tabindex="-1" role="dialog" aria-labelledby="emotionsModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <div class="row pt-3">
                  <div class="col-12 text-center">
                      <h4>Please select 18 emotions to continue.</h4>
                  </div>
              </div>
              <div class="row">
                  <div class="col-12 text-center">
                    <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal">OK</button>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
<script>
$("input:checkbox").on('click', function() {
  var $box = $(this);
  if ($box.is(":checked")) {
    // var group = "input:checkbox[name='" + $box.attr("name") + "']";
    // $(group).prop("checked", false);
    $box.prop("checked", true);
    $('.emotionCounter').html($("input:checkbox[id=emotionCheckbox]:checked").length);    
  } else {
    $box.prop("checked", false);
    $('.emotionCounter').html($("input:checkbox[id=emotionCheckbox]:checked").length);    
  }
  if($("input:checkbox[id=emotionCheckbox]:checked").length > 18) {     
        $('.emotionCounterWording').css({'color':'red'});
    } else {
        $('.emotionCounterWording').css({'color':'#168DD6'});
    }
});


window.onload = function() {
    $('.emotionCounter').html($("input:checkbox[id=emotionCheckbox]:checked").length);
}

$( "#emotionsForm" ).submit(function( event ) {
    if ($("input:checkbox[id=emotionCheckbox]:checked").length < 18 || $("input:checkbox[id=emotionCheckbox]:checked").length > 18) {
        event.preventDefault();
        $('#emotionsModal').modal();
    } else {
        $( "#emotionsForm" ).submit();
    }
});

$("#showAll").click(function(){
    $("#other").slideToggle("slow");
});
</script>
@endsection
