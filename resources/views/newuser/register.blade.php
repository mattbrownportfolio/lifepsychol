@extends('layouts.newuser', [
    'title' => 'Register'
])
@section('content')
<div class="container">
    <div class="be-content">
        @if (session('error'))
        <div class="alert alert-danger text-center mb-0 mt-4">
        <h3 class="my-0">{{ session('error') }}</h3>
        </div>       
        @endif
        @yield('content')
    </div>
    <div class="row mb-2">
        <div class="col-12 text-center">
            <h1>Register with Smart Tracker</h1>
            <h3>Step 1</h3>
            <div class="userInstruction userSelection">
                <p>In order to direct you to the correct area of the application, please let us know if you are a patient or a healthcare professional
                </p>
            </div>
        </div>
    </div>
    <div class="row userSelection pt-5">
        <div class="col-6 offset-3 col-md-4 offset-md-4 text-center userSelectionBox" id="patientToggle"><h3 class="mb-1">Non-Healthcare Professional</h3></div>
    </div>
    <div class="row mb-5 userSelection pt-5">
        <div class="col-6 offset-3 col-md-4 offset-md-4 text-center userSelectionBox" id="professionalToggle"><h3 class="mb-1">Healthcare Professional</h3></div>
    </div>
    
    @include('newuser.includes.hcprof')
    
    @include('newuser.includes.patient')
    <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Register</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Please fill out this form to register your details. If you have been through a traumatic experience in your life, which may include receiving a diagnosis for a medical condition/illness(es) and you wish to say, please fill in your details.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
@section('scripts')
<script>
    let passwordCheck = false;
    let termsCheck = false;
    let illnessCount = 1;
    function illnessTemplate(count) {
        if(count == 1) {
            let message = 'first';
        } else {
            let message = 'not first';
        }
       return `<div class="row pb-2" id="illnesses${count}">
                        <textarea class="form-control addEmotion mx-3" name="condition[${count}]" id="" cols="30" rows="5"></textarea>
                        <div class="col-12">
                            <p class="registerText text-center py-3"t">${count == 1 ? 'Please add any additional life events here. If you wish to add any more, please click the plus button' : ''}</p>
                        </div>                      
                        <datalist id="emotions" name="emotion">
                            @foreach ($allConditions as $condition)
                                <option>{{ $condition->condition }}</option>
                            @endforeach
                        </datalist >
                        <div class="col-12">
                            <p class="registerText">When approximately did this happen? </p>
                        </div>
                        <div class="row px-3 w-100">
                            <div class="col-6 col-md-4 pl-3">
                                <label for="years" class="registerText pl-2">Years</label>
                                <input name="years[${count}]" type="text" class="form-control w-50 w-xl-25 w-lg-25 w-md-25" style="float:left">
                            </div>
                            <div class="col-6 col-md-4 pl-0">
                                <label for="months" class="registerText pl-2">Months</label>
                                <input name="months[${count}]" type="text" class="form-control w-50 w-xl-25 w-lg-25 w-md-25" style="float:left">
                            </div>                        
                            <div class="col-1 offset-0 offset-md-3 d-none d-md-block" style="margin-left: 24%; padding-left: 4%;">
                                <span class="plus" id="removeIllness" onClick='removeIllnessEntry(0)'>&minus;</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-1 offset-0 offset-md-3 row d-block d-md-none text-right">
                        <div class="col-12" style="padding-right: 1.8rem;">
                            <span class="plus" id="removeIllness" onClick='removeIllnessEntry(0)'>&minus;</span>
                        </div>
                    </div>`
    }
    $(document).ready(function(){
            $(".plus").css('cursor', 'pointer');            
            
        })
//check passwords
$("#patientPassword1").on("keyup", function () {
    if($(this).val().length < 1) {
        passwordCheck = false;
        enablePatientNextButton();
        return;
    }
    if ($(this).val() != $("#patientPassword2").val()) {
            $("#patientPasswordWarning").show();
            passwordCheck = false;
            console.log(passwordCheck);
    } else {
            $("#patientPasswordWarning").hide();
            passwordCheck = true;
            console.log(passwordCheck);
    }
    enablePatientNextButton();
});

$("#patientPassword2").on("keyup", function () {
    if($(this).val().length < 1) {
        passwordCheck = false;
        enablePatientNextButton();
        return;
    }
    if ($("#patientPassword1").val() != $(this).val()) {
            $("#patientPasswordWarning").show();
            passwordCheck = false;           
    } else {
            $("#patientPasswordWarning").hide();
            passwordCheck = true;           
    }
    enablePatientNextButton();
});


$("#professionalPassword1").on("keyup", function () {
    if($(this).val().length < 1) {
        passwordCheck = false;
        enablePatientNextButton();
        return;
    }
    if ($(this).val() != $("#professionalPassword2").val()) {
            $("#passwordWarning").show();
            passwordCheck = false;
    } else {
            $("#passwordWarning").hide();
            passwordCheck = true;
    }
    enablePatientNextButton();
});

$("#professionalPassword2").on("keyup", function () {
    if($(this).val().length < 1) {
        passwordCheck = false;
        enablePatientNextButton();
        return;
    }
    if ($("#professionalPassword1").val() != $(this).val()) {
            $("#passwordWarning").show();
            passwordCheck = false;
    } else {
            $("#passwordWarning").hide();
            passwordCheck = true;
    }
    enablePatientNextButton();
});

$("#professionalPassword1").on("focusout", function () {
    if($(this).val().length < 1) {
        passwordCheck = false;
        enableProfessionalNextButton();
        return;
    }
    if ($(this).val() != $("#professionalPassword2").val()) {
            $("#professionalPasswordWarning").show();
            passwordCheck = false;
    } else {
            $("#professionalPasswordWarning").hide();
            passwordCheck = true;
    }
    enableProfessionalNextButton();
});

$("#professionalPassword2").on("keyup", function () {
    if($(this).val().length < 1) {
        passwordCheck = false;
        enableProfessionalNextButton();
        return;
    }
    if ($("#professionalPassword1").val() != $(this).val()) {
            $("#professionalPasswordWarning").show();
            passwordCheck = false;
    } else {
            $("#professionalPasswordWarning").hide();
            passwordCheck = true;
    }
    enableProfessionalNextButton();
});
//select drop down
    $(document).ready(function() {
        $('.dropdownSelect').select2();        
    });
// Date Picker
$(function() {
  $('input[name="dob"]').daterangepicker({
    autoApply: true,
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    locale: {
            format: 'DD/MM/YYYY'
        }
  });
});
$(document).ready(function() {
    $('#dateOfBirth').val('');        
});

//show conditions
$(document).ready(function() {
    $('input[type="radio"]').click(function() {
        var radios = document.getElementsByName('conditionCheck');
        for (var i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
               if(radios[i].value === '1') {
                $(".registerIllness").show("fast");
               } else {
                $(".registerIllness").hide("fast");
               }
            }
        }
   });
});
//show next button
$(document).ready(function() {
    $('input[type="checkbox"]').click(function() {
        if($('#patientConfirmTC').prop('checked')) {
            termsCheck = true;
        } else {
            termsCheck = false;
        }
        enablePatientNextButton();
   });
});
function enablePatientNextButton() {
    if((termsCheck == true) && (passwordCheck == true)) {
        $("#patientNextButton").removeClass("disabled");
        $("#patientNextButton").prop('disabled', false);
    } else {
        $("#patientNextButton").addClass("disabled");
        $("#patientNextButton").prop('disabled', true);
    }
}

$(document).ready(function() {
    $('input[type="checkbox"]').click(function() {
        if($('#professionalConfirmTC').prop('checked')) {
            termsCheck = true;
        } else {
            termsCheck = false;
        }
        enableProfessionalNextButton();
   });
});

function enableProfessionalNextButton() {
    if((termsCheck == true) && (passwordCheck == true)) {
            $("#professionalNextButton").removeClass("disabled");
            $("#professionalNextButton").prop('disabled', false);
    } else {
            $("#professionalNextButton").addClass("disabled");
            $("#professionalNextButton").prop('disabled', true);
    }
}

function addIllnessEntry() {
    let illnessRows = $("#illnessRows");
    illnessRows.append(illnessTemplate(illnessCount));
    illnessCount++;
}

function removeIllnessEntry(id) {
    let illnessItem = $("#illnesses" + id + "");
    illnessItem.remove();
}

$(document).ready(function(){
  $("#professionalToggle").click(function(){
    $("#professionalData").show();
    $(".userSelection").hide();
  });
  $("#patientToggle").click(function(){
    $("#patientData").show();
    $(".userSelection").hide();
  });
});

function showPatientPassword1() {
  var x = document.getElementById("patientPassword1");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function showPatientPassword2() {
  var x = document.getElementById("patientPassword2");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

function showProfPassword1() {
  var x = document.getElementById("professionalPassword1");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function showProfPassword2() {
  var x = document.getElementById("professionalPassword2");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

$('#patientForm').submit(function (evt) {
    if (!document.getElementById('consent').checked) {
        $('#consentMessageModal').modal('show');
        evt.preventDefault();
    }
});

function submitWithoutConsent() {
    console.log('bloop');
    document.getElementById('patientForm').submit();
}
</script>
@endsection
