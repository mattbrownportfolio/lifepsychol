<div class="px-4" id="patientData" style="display: none">
    <div class="userInstruction">
        <p>There are 3 easy steps to create your profile. After creating your profile, you can start using the tracker straight away.
        </p>
    </div>
    <form action="{{ url('/register') }}" method="POST" id="patientForm">
    @csrf        
        <div class="row">
            <div class="col-12 form-group">
                <label for="firstName" class="registerText mb-0">First Name</label>
                <input style="height:50px" type="text" name="firstName" class="form-control registerInputBoxes" required>
            </div>
        </div>
        <div class="row">
            <div class="col-12 form-group">
                <label for="surname" class="registerText mb-0">Surname</label>
                <input style="height:50px" type="text" name="surname" class="form-control registerInputBoxes" required>
            </div>
        </div>
        <div class="row">
            <div class="col-12 form-group">
                <label for="location" class="registerText mb-0">Your home county</label>
                <input style="height:50px" type="text" name="location" class="form-control registerInputBoxes" required>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 form-group">
                <label for="dob" class="registerText mb-0">Date of Birth</label>
                <input style="height:50px" type="text" value="" name="dob" class="form-control registerInputBoxes" id="dateOfBirth" required>
            </div>
            <div class="col-12 col-md-6 form-group">
                <label for="gender" class="registerText mb-0">Gender</label>
                <select class="form-control registerInputBoxes" id="gender" name="gender" required>
                    <option value="" selected disabled>Please select</option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                    <option value="3">Other / Prefer not to say</option>
                </select>
            </div>
        </div>
        <div class="row py-4">
            <div class="col-12 form-group">
                <p class="registerText mb-0">
                    Smart Tracker has been developed with the aim of recognising the importance of emotions resulting from a life incident (physical or mental). We believe this is a significant and key step in supporting the health and wellbeing of individuals. 
                <br><br>
                Have you experienced one or more significant life event/experience? (This may include witnessing an event or you or a loved one received news of a diagnosis for a medical condition/illness(es))   
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <div class="form-check-inline">
                    <label class="form-check-label registerText pr-5" style="vertical-align: middle">
                      <input type="radio" value="1" class="form-check-input registerInputRadio mr-2" name="conditionCheck" style="vertical-align: middle">Yes
                    </label>
                </div>
            </div>
            <div class="col-3">
                <div class="form-check-inline">
                    <label class="form-check-label registerText pl-5" style="vertical-align: middle">
                      <input type="radio" value="0" class="form-check-input registerInputRadio mr-2" name="conditionCheck" style="vertical-align: middle" checked="checked">No
                    </label>
                </div>
            </div>
        </div>
        <div class="row mt-4 pt-3 registerIllness" style="display: none;">
            <div class="col-12">
                <label for="illness" class="registerText">Please give details of the diagnosis or event. If you wish to add multiple, please click the plus icon at the bottom right.</label>
                   <div id="illnessRows">
                    <div class="row pb-2" id="illnesses0">
                        {{-- <input class="form-control addEmotion mx-3" list="conditions" name="condition[0]"> --}}
                        <textarea class="form-control addEmotion mx-3" name="condition[0]" id="" cols="30" rows="5"></textarea>
                        <datalist id="conditions">
                            @foreach ($allConditions as $condition)
                                <option>{{ $condition->condition }}</option>
                            @endforeach
                        </datalist>
                        <div class="col-12">
                            <p class="registerText">When approximately did this happen? </p>
                        </div>
                        <div class="row px-3 w-100">
                            <div class="col-6 col-md-4 pl-3">
                                <label for="years" class="registerText pl-2">Years</label>
                                <input name="years[0]" type="text" class="form-control w-50 w-xl-25 w-lg-25 w-md-25" style="float:left">
                            </div>
                            <div class="col-6 col-md-4 pl-0">
                                <label for="months" class="registerText pl-2">Months</label>
                                <input name="months[0]" type="text" class="form-control w-50 w-xl-25 w-lg-25 w-md-25" style="float:left">
                            </div>                        
                            <div class="col-1 offset-0 offset-md-3 d-none d-md-block" style="margin-left: 24%; padding-left: 4%">
                                <span class="plus" id="removeIllness" onClick='removeIllnessEntry(0)'>&minus;</span>
                            </div>
                        </div>
                    </div>
                    <div class="row d-block d-md-none text-right">
                        <div class="col-12" style="padding-right: 1.9rem;">
                            <span class="plus" id="removeIllness" onClick='removeIllnessEntry(0)'>&minus;</span>
                        </div>
                    </div>
                   </div>
            </div>
            <div class="text-right col-12">
                <span class="plus pr-3" id="addAnotherIllness" onClick='addIllnessEntry()'>+</span>
            </div>
        </div>
        <div class="row pt-4">
            <div class="col-12 form-group">
                <label for="email" class="registerText mb-0">Email</label>
                <input style="height:50px" type="email" name="email" class="form-control registerInputBoxes" required>
            </div>
        </div> 
        <div class="row pt-4">
            <div class="col-12 form-group">
                <label for="patientPassword1" class="registerText mb-0">Password</label>
                <input style="height:50px" type="password" id="patientPassword1" name="patientPassword1" class="form-control registerInputBoxes" required>
                <div class="pt-1" onclick="showPatientPassword1()">click here to show/hide Password</div>
            </div>
        </div>
        <div class="row pt-4">
            <div class="col-12 form-group">
                <label for="patientPassword2" class="registerText mb-0">Retype Password <span style="display: none; font-size: 15px; color: red;" id="patientPasswordWarning">Passwords Do Not Match</span></label>
                <input style="height:50px" type="password" id="patientPassword2" name="patientPassword2" class="form-control registerInputBoxes" required>
                <div class="pt-1" onclick="showPatientPassword2()">click here to show/hide Password</div>
            </div>
        </div>
        <div class="row pt-4">
            <div class="col-12 form-group">
                <label for="code" class="registerText mb-0">Enter Access Code</label>
                <label for="code">If you have not been given a code, please click <a href="#">here</a> to obtain one.</label>
                <input style="height:50px" type="text" name="patientcode" class="form-control registerInputBoxes">
            </div>
        </div>      
        <div class="row pt-4">
            <div class="col-12 col-md-6 form-group">
                <div class="form-check pr-5">
                    <input type="checkbox" id="patientConfirmTC" class="form-check-input registerInputRadio" name="optradio" style="vertical-align: middle">
                    <label class="form-check-label registerTextSmall" style="vertical-align: middle; padding-left: 20px; ">                      
                      <p>I have read and accept the <a href="{{ route('terms') }}" target="_blank">Terms and Conditions</a></p>
                    </label>
                </div>
            </div>
            <div class="col-12 col-md-6 form-group">
                <div class="form-check pr-5">
                    <input type="checkbox" id="consent" class="form-check-input registerInputRadio mr-2" name="consent" style="vertical-align: middle" value="1">
                    <label class="form-check-label registerTextSmall" style="vertical-align: middle; padding-left: 20px;">                      
                      <p>Please tick this box if you would be happy to share your information with a healthcare professional</p>
                    </label>
                </div>
            </div>
        </div>
        <div class="modal fade" id="consentMessageModal" role="dialog">
            @include('includes.modals.consentMessageModal')
        </div>
        <div class="row pt-4">
            <div class="col-12 form-group">
                <button  id="patientNextButton" type="submit" class="btn btn-primary btn-block loginButton disabled" style="background-color: #344EA9; border: none;" disabled>Next</button >
            </div>
        </div>
    </form>
</div>
