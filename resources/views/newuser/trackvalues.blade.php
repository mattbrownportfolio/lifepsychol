@extends('layouts.newuser', [
    'title' => 'Track Values'
])
@section('content')
<div class="container">
  <div class="row mb-2">
    <div class="col-12 text-center">
        <h1>Personal Values</h1>
        <h3>Step 3</h3>
    </div>
</div>
<div class="px-4">
    <div class="userInstruction">
        <p>Like the previous step, this final step is part of the personalisation of the {{config('app.name')}}. Please select from the following list between 5 and 12 areas or aspects of life that are of particularly high value to you.</p>
    </div>
    <div class="stickyHeaderEmotion">
      <h2 class="text-center pt-2 valueCounterWording"><span class="valueCounter">0</span> Selected</h2>
    </div>
    <form action="{{ route('trackvalues.store') }}" method="POST" id="valuesForm">
        @csrf
        @foreach($allValues as $value)
            <div class="row pt-2">
                <div class="col-8 registerText">{{ ucfirst($value->name) }}</div>
                <div class="col-2 offset-1 text-center">
                    <input type="checkbox" id="valueCheckbox" name="value[{{ $value->id }}]" class="registerInputRadio">
                </div>
            </div>
        @endforeach   
        <h2 class="text-center pt-2 valueCounterWording"><span class="valueCounter">0</span> Selected</h2>  
        <div class="userInstruction">
          <p>If there is another area of your life you wish to include which isn't mentioned above, please click Add New Value to enter your own value(s). Once you have selected all your values, please click Next to continue</p>
          <div class="row pt-4">
            <div class="col-4 offset-2">
                <a href="{{ route('addvalues') }}" class="btn btn-primary btn-block loginButton">Add New Value</a>                
            </div>
            <div class="col-4 form-group">
              <button href="#" class="btn btn-primary btn-block loginButton">Next</button>
          </div>
        </div>
      </div>         
    </form>
</div>
<div class="modal fade" id="valuesModal" tabindex="-1" role="dialog" aria-labelledby="valuesModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="row pt-5">
              <div class="col-12 text-center">
                  <h4>Please select between 5 and 12 values to continue.</h4>
              </div>
          </div>
          <div class="row">
              <div class="col-12 text-center">
                <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal">OK</button>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Choosing Values</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            Please choose between 5 and 12 values to track. These are values you will use later on when scoring.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
    <script>
      $("input:checkbox").on('click', function() {
        var $box = $(this);
        if ($box.is(":checked")) {         
          $('.valueCounter').html($("input:checkbox[id=valueCheckbox]:checked").length);
        } else {
          $('.valueCounter').html($("input:checkbox[id=valueCheckbox]:checked").length);
        }  
        if($("input:checkbox[id=valueCheckbox]:checked").length < 5 || $("input:checkbox[id=valueCheckbox]:checked").length > 12) {     
          $('.valueCounterWording').css({'color':'red'});
        } else {
          $('.valueCounterWording').css({'color':'#168DD6'});
        }
      });    
      
        $( "#valuesForm" ).submit(function( event ) {
            if ($("input:checkbox[id=valueCheckbox]:checked").length < 5 || $("input:checkbox[id=valueCheckbox]:checked").length > 12) {
                event.preventDefault();
                $('#valuesModal').modal();
            } else {
                $( "#valuesForm" ).submit();
            }
        });

        $( document ).ready(function() {
          if($("input:checkbox[id=valueCheckbox]:checked").length < 5) {     
            $('.valueCounterWording').css({'color':'red'});
          }
        });

        window.onload = function() {
          $('.valueCounter').html($("input:checkbox[id=valueCheckbox]:checked").length);          
        }

    </script>
@endsection
