<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name') }}</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <div class="flex-center position-ref loginMainBody">
            <div class="content">
                <div class="title m-b-md">
                    <img class="img-fluid px-4 pb-4" src="{{ asset('img/mainlogo.svg') }}" alt="" width="400px">
                </div>
                <form method="POST" action="{{ route('login') }}" style="padding-top: 100px">
                    @csrf
                    <div class="row">
                        <div class="col-10 offset-1">
                            <input id="email" type="email" class="form-control loginFormInput @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Username" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-10 offset-1 pt-4">
                            <input id="password" type="password" placeholder="Password" class="form-control loginFormInput @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    @if (session('error'))
                    <div class="alert alert-danger text-center mb-0 mt-4">
                      <h3 class="my-0">{{ session('error') }}</h3>
                    </div>                   
                    @endif
                    <div class="row">
                        <div class="col-10 offset-1 pt-4">
                            @if (Auth::user())
                            <a class="btn btn-primary btn-block loginButton" role="button" href="{{ route('home')}}">
                                    Home
                                </a>
                            @else
                                <button class="btn btn-primary btn-block loginButton" type="submit">
                                    Log In
                                </button>
                            @endif
                        </div>
                    </div>
                </form>
            <p class="pt-4">Don't have an account? <a href="{{ url('/register') }}" style="font-weight: bold">Sign Up</a></p>
            <p>Forgotten password? <a href="{{ url('/password/reset') }}" style="font-weight: bold">Click here</a></p>
            </div>
        </div>
    </body>
</html>
