@component('mail::message')
# Reminder

This is a reminder to say you are now due to complete your Smart Tracker.

@component('mail::button', ['url' => ''])
Click here to complete your tracker
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
