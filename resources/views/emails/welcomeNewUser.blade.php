@component('mail::message')
# Welcome {{ $name['firstname'] }}

You have now been registered for Smart Tracker. Your code is <br> <h1>{{ $code }}</h1> <br> Please give this code to your patients when they are ready to sign up.  <br>

Please click the link below to set your account password and get access to your account :

@component('mail::button', ['url' => $url])
Set Password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
