@component('mail::message')
Hi, {{ $name }}. This email is you let you know you are nearing your allocated patient limit on your free account. If you require more patient codes, please visit <a href="'">our website here</a> to purchase more.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
