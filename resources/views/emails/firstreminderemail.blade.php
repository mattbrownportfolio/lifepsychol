@component('mail::message')
# 1st reminder

You are coming up on something

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
