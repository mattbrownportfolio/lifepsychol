@component('mail::message')
<p class="font-weight-bold">{{ $companyName }}</p> is nearing their limit of alloted codes. Please encourage them to purchase more.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
