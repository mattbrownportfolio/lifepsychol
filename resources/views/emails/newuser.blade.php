@component('mail::message')
# New user

A new user has submitted the following illnesses:
<ul>
    @foreach ($conditions as $condition)
      <li>{{ $condition }}</li>  
    @endforeach
</ul>


@component('mail::button', ['url' => $url, 'colour' => 'primary'])
Go to illness list
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
