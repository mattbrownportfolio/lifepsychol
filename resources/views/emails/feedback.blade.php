@component('mail::message')
# Feedback Received

{{ $feedback }}

<br>
{{ config('app.name') }}
@endcomponent
