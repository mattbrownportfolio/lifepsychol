<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>{{ config('app.name') }}</title>
    <link href="{{ asset('lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/material-design-icons/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/jquery.vectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/jqvmap/jqvmap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/datatables/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet" rel="stylesheet">
    <link href="{{ asset('lib/datatables/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/AdminApp.css') }}" />
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      @include('includes.admin.topNav')
      @include('includes.admin.sideNav')
      <div class="be-content">
        @if (session('status'))
        <div class="alert alert-success text-center mb-0 mt-4">
          <h3 class="my-0">{{ session('status') }}</h3>
        </div>
        @elseif (session('delete'))
        <div class="alert alert-danger text-center mb-0 mt-4">
          <h3 class="my-0">{{ session('delete') }}</h3>
        </div>
        @endif
        @yield('content')
      </div>
    </div>
    <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('lib/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
    {{-- Datatables jquery --}}
    <script src="{{ asset('lib/datatables/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('lib/datatables/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('lib/datatables/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('lib/datatables/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('lib/datatables/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('lib/datatables/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    {{--    Graph jquery--}}
    <script src="{{ asset('lib/jquery-flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('lib/jquery-flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('lib/jquery-flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('lib/jquery-flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('lib/jquery-flot/plugins/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('lib/jquery-flot/plugins/curvedLines.js') }}"></script>
    <script src="{{ asset('lib/jquery-flot/plugins/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ asset('lib/jquery.sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('lib/countup/countUp.min.js') }}"></script>
    <script src="{{ asset('lib/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('lib/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('lib/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('/lib/jquery.sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
    <script src="{{ asset('js/admin.js') }}"></script>

    <script type="text/javascript">
    $.fn.niftyModal('setDefaults',{
      	overlaySelector: '.modal-overlay',
      	contentSelector: '.modal-content',
      	closeSelector: '.modal-close',
        classAddAfterOpen: 'modal-show',
        backdrop: 'static',
        keyboard: false
      });
      $(document).ready(function(){
      	//-initialize the javascript
      	App.init();
        App.dataTables();

      });
    </script>
    @yield('scripts')

  </body>
</html>
