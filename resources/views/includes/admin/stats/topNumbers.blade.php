<div class="row">
    <div class="col-12 col-lg-6 col-xl-4">
        <div class="widget widget-tile">
            <div class="chart sparkline" id="spark1">
                <canvas width="85" height="35" style="display: inline-block; width: 85px; height: 35px; vertical-align: top;"></canvas>
            </div>
            <div class="data-info">
                <div class="desc">Total Number of Users</div>
                <div class="value">
                    <span class="indicator indicator-equal mdi mdi-chevron-right"></span>
                    <span class="number" data-toggle="counter" data-end="{{ $allUsers->count() }}">{{ $allUsers->count() }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-6 col-xl-4">
        <div class="widget widget-tile">
            <div class="chart sparkline" id="spark2">
                <canvas width="85" height="35" style="display: inline-block; width: 85px; height: 35px; vertical-align: top;"></canvas>
            </div>
            <div class="data-info">
                <div class="desc">Avg usage per quarter</div>
                <div class="value">
                    <span class="indicator indicator-equal mdi mdi-chevron-right"></span>
                    <span class="number" data-toggle="counter" data-end="{{ round($allSessions->count() / $allUsers->count()) }}">{{ round($allSessions->count() / $allUsers->count()) }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-6 col-xl-4">
        <div class="widget widget-tile">
            <div class="chart sparkline" id="spark3">
                <canvas width="85" height="35" style="display: inline-block; width: 85px; height: 35px; vertical-align: top;"></canvas>
            </div>
            <div class="data-info">
                <div class="desc">% of active users</div>
                <div class="value">
                    <span class="indicator indicator-equal mdi mdi-chevron-right"></span>
                    <span class="number" data-toggle="counter" data-end="{{$activeUsers->count()}}">{{$activeUsers->count()}}</span>
                </div>
            </div>
        </div>
    </div>
</div>
