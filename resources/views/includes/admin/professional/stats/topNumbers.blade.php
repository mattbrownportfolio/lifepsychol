<div class="row">
    <div class="col-12 col-lg-6 col-xl-4">
        <div class="widget widget-tile">
            <div class="chart sparkline" id="spark1">
                <canvas width="85" height="35" style="display: inline-block; width: 85px; height: 35px; vertical-align: top;"></canvas>
            </div>
            <div class="data-info">
                <div class="desc">Codes Used</div>
                <div class="value">
                    <span class="indicator indicator-equal mdi mdi-chevron-right"></span>
                    <span class="number" data-toggle="counter" data-end="{{ $profCount }}">{{ $profCount }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-6 col-xl-4">
        <div class="widget widget-tile">
            <div class="chart sparkline" id="spark2">
                <canvas width="85" height="35" style="display: inline-block; width: 85px; height: 35px; vertical-align: top;"></canvas>
            </div>
            <div class="data-info">
                <div class="desc">Total Available Company Codes</div>
                <div class="value">
                    <span class="indicator indicator-equal mdi mdi-chevron-right"></span>
                    <span class="number" data-toggle="counter" data-end="{{ $companyCodeCount->maximum_code_count }}">{{ $companyCodeCount->maximum_code_count }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-6 col-xl-4">
        <div class="widget widget-tile">
            <div class="chart sparkline" id="spark3">
                <canvas width="85" height="35" style="display: inline-block; width: 85px; height: 35px; vertical-align: top;"></canvas>
            </div>
            <div class="data-info">
                <div class="desc">Number of Patients Sharing Data</div>
                <div class="value">
                    <span class="indicator indicator-equal mdi mdi-chevron-right"></span>
                    <span class="number" data-toggle="counter" data-end="{{ $users->count() }}">{{ $users->count() }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
