<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header modal-header-colored">
      <h3 class="modal-title">Edit Healthcare Details</h3>      
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="row px-3">
      <div class="col-12">
        <h5>If you have changed your healthcare provider and have been given a new Membership Number, please enter it below, otherwise leave blank. Click cancel to remain with your current provider</h5>
      </div>
    </div>
  <form id="editFormHC" action="" method="post">
    @method('put')
      {{ csrf_field() }}
      <div class="modal-body form"> 
        <div class="row">
          <div class="col-12">
            <div class="form-group">
                <label for="patientcode"><h4>Membership Number</h4></label>
                <input class="form-control" type="text" name="patientcode" id="patientcode">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="form-group">
                <h4>Do you wish to share your details with your healthcare professional?</h4>
                <div class="form-check form-check-inline">
                  <input class="form-check-input registerInputRadioSmall" type="radio" name="consent" id="consentYes" value="1" {{ ($user->consent =="1")? "checked" : "" }}>
                  <label class="form-check-label" for="consentYes" style="padding-top: 6px"><h4>Yes</h4></label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input registerInputRadioSmall" type="radio" name="consent" id="consentNo" value="0" {{ ($user->consent =="0")? "checked" : "" }}>
                  <label class="form-check-label" for="consentNo" style="padding-top: 6px"><h4>No</h4></label>
                </div>
            </div>
          </div>
        </div>
      </div>
          <div class="modal-footer">
            <button class="btn btn-secondary modal-close" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-success modal-close" type="submit">Submit</button>
          </div>
    </form>
  </div>
</div>
