<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header modal-header-colored">
      <h3 class="modal-title">Edit Professional</h3>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <form id="editForm" action="" method="post">
    @method('put')
      {{ csrf_field() }}
      <div class="modal-body form">
        <div class="row">
          <div class="col-6">
              <div class="form-group">
                  <label for="firstname"><h4>First Name</h4></label>
                  <input class="form-control" type="text" name="firstname" id="firstname" required>
              </div>
          </div>
          <div class="col-6">
              <div class="form-group">
                  <label for="surname"><h4>Surname</h4></label>
                  <input class="form-control" type="text" name="surname" id="surname" required>
              </div>
          </div>
        </div>  
        <div class="row">
          <div class="col-6">
              <div class="form-group">
                  <label for="email"><h4>Email</h4></label>
                  <input class="form-control" type="text" name="email" id="email" required>
              </div>
          </div>
          <div class="col-6">
              <div class="form-group">
                  <label for="company"><h4>Company</h4></label>
                  <select class="form-control" id="company" name="company" required>
                      <option selected disabled>Choose Company</option>                                   
                      @foreach ($allCompanies as $company)
                          <option value="{{ $company->id }}">{{ $company->name }}</option>
                      @endforeach
                  </select>
              </div>
          </div>                        
        </div>
      <div class="row">
        <div class="col-6">
          <div class="form-group">
              <label for="code"><h4>Code</h4></label>
              <input class="form-control" type="text" name="code" id="code">
          </div>
        </div>                                                  
      </div>
          <div class="modal-footer">
            <button class="btn btn-secondary modal-close" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-success modal-close" type="submit">Submit</button>
          </div>
      </div>
    </form>
  </div>
</div>
