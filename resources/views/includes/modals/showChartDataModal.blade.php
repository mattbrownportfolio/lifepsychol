<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header modal-header-colored">
        <h3 class="modal-title text-center" id="title"></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="body" style="font-size: 1.2rem">
        <div class="row">
          <div class="col">
            Date: <p id="date"></p>
          </div>
          <div class="col">
            Score: <p id="score"></p>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            Note: <p id="note"></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  