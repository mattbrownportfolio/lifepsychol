<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header modal-header-colored">
      <h3 class="modal-title">Edit Emotion</h3>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <form id="editForm" action="" method="post">
    @method('put')
      {{ csrf_field() }}
      <div class="modal-body form"> 
        <div class="row">
          <div class="col-12">
            <div class="form-group">
                <label for="linkText"><h4>Emotion</h4></label>
                <input class="form-control" type="text" name="emotion" id="emotion">
            </div>
          </div>
      </div>
      </div>
          <div class="modal-footer">
            <button class="btn btn-secondary modal-close" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-success modal-close" type="submit">Submit</button>
          </div>
    </form>
  </div>
</div>
