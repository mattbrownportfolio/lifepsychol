<div class="modal-dialog" role="document">
    <div class="modal-content text-center">      
      <form id="deleteForm" action="" method="post">  
          {{ csrf_field() }}
          <div class="modal-body form">
              <h2>Are you sure?</h2>
              <div class="row pt-5">
                <div class="col-12 text-center"> 
                  <button class="btn btn-danger modal-close" type="submit">Delete</button>               
                  <button class="btn btn-secondary modal-close" type="button" data-dismiss="modal">Cancel</button>                                
                </div>
              </div>
        </form>
    </div>
  </div>
  