<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header modal-header-colored">
      <h3 class="modal-title">Edit Resource</h3>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <form id="editForm" action="" method="post">
    @method('put')
      {{ csrf_field() }}
      <div class="modal-body form"> 
        <div class="row">
          <div class="col-12">
            <div class="form-group">
                <label for="linkText"><h4>Link Text</h4></label>
                <input class="form-control" type="text" name="link_text" id="link_text">
            </div>
          </div>
      </div>
      <div class="row">
          <div class="col-6">
            <div class="form-group">
                <label for="linkUrl"><h4>Link Url</h4></label>
                <input class="form-control" type="text" name="link_url" id="link_url">
            </div>
          </div>
          <div class="col-6">
                <div class="form-group">
                    <label for="category"><h4>Category</h4></label>
                    <select class="custom-select form-control" id="category_id" name="category_id">              
                        @foreach ($allCategories as $category)                                    
                        <option value="{{ $category->id }}">{{ $category->name }}</option>                               
                        @endforeach
                    </select>
                </div>
          </div>
      </div>
      <div class="row">
          <div class="col">
            <div class="form-group">
                <label for="description"><h4>Description</h4></label>
                <textarea class="form-control" rows="8" id="description" name="description" class="textarea"></textarea >
            </div>
          </div>
      </div>       
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary modal-close" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-success modal-close" type="submit">Submit</button>
          </div>
    </form>
  </div>
</div>
