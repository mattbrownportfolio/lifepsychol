<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header modal-header-colored">
        <h3 class="modal-title text-center" id="title"></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="body" style="font-size: 1.2rem">
        <div class="row">
          <div class="col">
            Number of Emotions selected: <span id="count"></span>
          </div>
          <div class="col">
            Score for this value: <span id="score"></span>
          </div>
        </div>        
      </div>
    </div>
  </div>
  