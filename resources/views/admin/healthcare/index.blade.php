@extends('layouts.admin')
@section('content')
    <h1 class="text-center pt-5">Healthcare Professionals
        <button class="btn btn-space btn-success" data-toggle="modal" data-target="#resourceModal" type="button">
            Add New Healthcare Professional
          </button>
    </h1>
    <div class="col-12">
        <table class="table table-striped table-hover table-fw-widget text-center" id="table1">
            <thead>
            <tr>
                <th width="10%">Name</th>
                <th width="10%">Email</th>
                <th width="10%">Code</th>               
                <th width="10%">Codes Used</th>                
                <th width="15%">Company</th>
                <th width="10%">State</th>
                <th width="15%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($healthcareProfessionals as $professional) 
            
                <tr>
                    <td>{{ $professional['firstname'] . ' ' .  $professional['surname'] }}</td>
                    <td>{{ $professional['email'] }}</td>
                    <td>{{ $professional['code'] }}</td>                    
                    <td>{{ $professional['used'] }}</td>                    
                    <td>{{ $professional['company'] }}</td>
                    <td>{{ $professional['deleted_at'] != null ? 'Not Active' : 'Active' }}</td>
                    <td>
                        <form-group>
                            <button
                                class="btn btn-primary md-trigger"
                                type="button"
                                data-id="{{ $professional['id'] }}"
                                data-firstname="{{ $professional['firstname'] }}"
                                data-surname="{{ $professional['surname'] }}"
                                data-email="{{ $professional['email'] }}"
                                data-company="{{ $professional['company_id'] }}"    
                                data-code="{{ $professional['code'] }}"
                                data-maxcodes="{{ $professional['max_codes'] }}"
                                data-toggle="modal"
                                data-target="#edit-professional">Edit</button>
                            <button
                                class="btn btn-danger md-trigger"
                                type="button"
                                data-id="{{ $professional['id'] }}"
                                data-toggle="modal"
                                data-target="#delete-professional">Delete</button>
                        </form-group>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="modal fade" id="resourceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" id="exampleModalLongTitle">Add New Healthcare Professional</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.professionals.store')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="firstname"><h4>First Name</h4></label>
                                <input class="form-control" type="text" name="firstname" id="firstname" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="surname"><h4>Surname</h4></label>
                                <input class="form-control" type="text" name="surname" id="surname" required>
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="email"><h4>Email</h4></label>
                                <input class="form-control" type="text" name="email" id="email" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="company"><h4>Company</h4></label>
                                <select class="form-control" id="company" name="company" required>
                                    <option value="" selected disabled>Choose Company</option>                                   
                                    @foreach ($allCompanies as $company)
                                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    
        <div class="modal fade" id="edit-professional" role="dialog">
            @include('includes.modals.editProfessionalModal')
        </div>
        <div class="modal fade" id="delete-professional" role="dialog">
            @include('includes.modals.deleteProfessionalModal')
        </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $('#table1').DataTable();
        });
        $('#edit-professional').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let modal = $(this);
            let firstname = button.data('firstname');
            let surname = button.data('surname');
            let company = button.data('company'); 
            let email = button.data('email');   
            let code = button.data('code');  
            let maxcodes = button.data('maxcodes');            
            let id = button.data('id');
            modal.find('.modal-body #firstname').val(firstname);
            modal.find('.modal-body #surname').val(surname);
            modal.find('.modal-body #company').val(company); 
            modal.find('.modal-body #code').val(code);      
            modal.find('.modal-body #email').val(email);  
            modal.find('.modal-body #codeCount').val(maxcodes);         
            const url = "{{ route('admin.professionals.update', ':id') }}"
            $('#editForm').attr("action", url.replace(':id', id));
        });
        $('#delete-professional').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let modal = $(this);
            let id = button.data('id');
            const url = "{{ route('admin.professionals.delete', ':id') }}"
            $('#deleteForm').attr("action", url.replace(':id', id));
        })
    </script>

@endsection
