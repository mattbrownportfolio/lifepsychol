@extends('layouts.admin')
@section('content')
<div class="row main-content container-fluid">
  <div class="col-12">
      <h3 class="pb-5">{{ Auth::user()->role_id == 1 ? $user->patientcode : $user->firstname . ' ' . $user->surname }}'s Life Events <br><br><small>Here are the details left by this patient. If you wish to leave any tags against their Life Event to aid in searching, you may do so here</small></h3>
  </div>
</div>
<div class="col-10 offset-1">
  <table class="table table-striped table-hover table-fw-widget text-center" id="table1">
    <thead>
      <tr>            
          <th width="40%">Condition</th>  
          <th width="40%">Tags</th>
          <th width="20%">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($userConditionsArray as $condition)
      <tr>     
        <td>{{ $condition['condition'] }}</td>       
        <td>
            @foreach($condition['tags'] as $tags)
            {{$tags}}{{ $loop->last ?  ' ' : ',' }}
            @endforeach
        </td> 
        <td>
        <button
            class="btn btn-success md-trigger"
            type="button"
            data-id="{{ $condition['condition_id'] }}"
            data-user_id="{{ $user->id }}"
            data-tags="{{ $condition['tags'] }}"
            data-toggle="modal"
            data-target="#edit-userCondition">Edit Tags</button>
            <button
            class="btn btn-primary md-trigger"
            data-toggle="modal" 
            data-target="#resourceModal" 
            data-id="{{ $condition['condition_id'] }}"
            data-user_id="{{ $user->id }}"
            data-tags="{{ $condition['tags'] }}"
            type="button">Add Tags</button>
        </td>
      </tr>
      @endforeach  
    </tbody>
  </table>
  <div class="row">
    <div class="col-4 offset-4 pb-5">
        <a href="{{ route('dashboard.patients.index') }}" class="btn btn-primary btn-block">
            BACK
        </a>
    </div>
</div>
  <div class="modal fade" id="resourceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header modal-header-colored">
          <h3 class="modal-title">Add Tags</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        
      <form id="addnewForm" action="{{ route('dashboard.patients.store') }}" method="post">      
          {{ csrf_field() }}
          <input type="hidden" id="user_id" value="{{$user->id}}" name="user_id">
          <input type="hidden" id="condition_id" value="{{$condition['condition_id']}}" name="condition_id">
          <div class="modal-body form">
            <div class="row">
              <div class="col-12">
                  <div class="form-group"><label><h4>Tags. If entering multiple, separate with a comma</h4></label>
                      <textarea class="form-control" type="text" name="tags" id="tags"></textarea>
                  </div>
              </div>          
            </div>  
            <div class="modal-footer">
              <button class="btn btn-secondary modal-close" type="button" data-dismiss="modal">Cancel</button>
              <button class="btn btn-success modal-close" type="submit">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
</div>

</div>

{{-- edit question modal --}}
<div class="modal fade" id="edit-userCondition" role="dialog">
  @include('includes.modals.editUserConditionModal')
</div>
@endsection



@section('scripts')
<script>
  $(document).ready( function () {
      $('#table1').DataTable();
  });
$('#edit-userCondition').on('show.bs.modal', function (event) {
  let button = $(event.relatedTarget);
  let modal = $(this);
  let id = button.data('id');  
  let tags = button.data('tags');   
  let user_id = button.data('user_id');     
  modal.find('.modal-content #user_id').val(user_id);
  modal.find('.modal-content #tags').val(tags);
  modal.find('.modal-content #condition_id').val(id);
  const url = "{{ route($path, ':id') }}"
  $('#editForm').attr("action", url.replace(':id', id));
});
</script>

@endsection
