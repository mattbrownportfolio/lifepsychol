@extends('layouts.admin')
@section('content')
<h1 class="text-center pt-5">Resources
<button class="btn btn-space btn-success" data-toggle="modal" data-target="#resourceModal" type="button">
  Add New Resources
</button>
</h1>
<div class="col-12">
  <table class="table table-striped table-hover table-fw-widget" id="table1">
    <thead>
      <tr>
          <th width="10%">Link Text</th>
          <th width="20%">Link URL</th>
          <th width="10%">Times Visited</th>
          <th width="10%">Category</th>
          <th width="40%">Description</th>
          <th width="10%">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($allResources as $resource)
      <tr>

        <td>{{ $resource->link_text }}</td>
        <td>{{ $resource->link_url }}</td>
        <td>{{ $resource->click_count ? $resource->click_count : 0 }}</td>
        <td>{{ $resource->category->name }}</td>
        <td>{{ $resource->description }}</td>
        <td>
          <form-group>
            <button
            class="btn btn-primary md-trigger"
            type="button"
            data-link_text="{{ $resource->link_text}}"
            data-link_url="{{ $resource->link_url}}"
            data-category="{{ $resource->category_id }}"
            data-description="{{ $resource->description }}"
            data-id="{{ $resource->id }}"
            data-toggle="modal"
            data-target="#edit-resource">Edit</button>
            <button
            class="btn btn-danger md-trigger"
            type="button"
            data-id="{{ $resource->id }}"
            data-toggle="modal"
            data-target="#delete-resource">Delete</button>
        </form-group>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<div class="modal fade" id="resourceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Add New Resource</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="{{ route('admin.resources.store')}}" method="POST">
            @csrf
              <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                        <label for="linkText"><h4>Link Text</h4></label>
                        <input class="form-control" type="text" name="linkText" id="linkText">
                    </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                        <label for="linkUrl"><h4>Link Url</h4></label>
                        <input class="form-control" type="text" name="linkUrl" id="linkUrl">
                    </div>
                  </div>
                  <div class="col-6">
                        <div class="form-group">
                            <label for="category"><h4>Category</h4></label>
                            <select class="form-control" id="category" name="category">
                              <option selected disabled>Choose Category</option>
                                @foreach ($allCategories as $category)
                                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col">
                    <div class="form-group">
                        <label for="description"><h4>Description</h4></label>
                        <textarea class="form-control" rows="8" id="description" name="description" class="textarea"></textarea >
                    </div>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" >Save changes</button>
            </div>
        </form>
        </div>
    </div>
</div>
{{-- edit question modal --}}
<div class="modal fade" id="edit-resource" role="dialog">
  @include('includes.modals.editResourceModal')
</div>

{{-- delete question modal --}}
<div class="modal fade" id="delete-resource" role="dialog">
  @include('includes.modals.deleteResourceModal')
</div>
@endsection



@section('scripts')
<script>
  $(document).ready( function () {
      $('#table1').DataTable();
  });
$('#edit-resource').on('show.bs.modal', function (event) {
  let button = $(event.relatedTarget);
  let modal = $(this);
  let link_text = button.data('link_text');
  let link_url = button.data('link_url');
  let category = button.data('category');
  let description = button.data('description');
  let id = button.data('id');
  modal.find('.modal-content #link_text').val(link_text);
  modal.find('.modal-body #link_url').val(link_url);
  modal.find('.modal-body #category_id').val(category);
  modal.find('.modal-body #description').val(description);
  const url = "{{ route('admin.resources.update', ':id') }}"
  $('#editForm').attr("action", url.replace(':id', id));
});
$('#delete-resource').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let modal = $(this);
    let id = button.data('id');
    const url = "{{ route('admin.resources.delete', ':id') }}"
    $('#deleteForm').attr("action", url.replace(':id', id));
  })
</script>

@endsection
