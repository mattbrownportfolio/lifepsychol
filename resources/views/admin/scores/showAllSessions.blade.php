@extends('layouts.admin', [
    'title' => 'Patient Sessions'
])
@section('content')
<div class="row main-content container-fluid">
    <div class="col-12">
        <h3 class="pb-5">{{ $user->firstname . ' ' . $user->surname }}'s Sessions <br><br><small>Here is how your patient has scored each of their sessions. The graph below charts their score's progress, and each of the values beneath the graph can be toggled off and on to show certain scores in the graph.</small></h3>
    </div>
</div>
    <div class="col-12 pb-5">
        <table class="table table-striped table-hover table-fw-widget text-center" id="table1">
            <thead class="darkTableHead">
            <tr>
                <th width="40%">Date Of Session</th>    
                <th width="40%">Scores</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($allUserSessions as $session)
                <tr>
                    <td>{{ \Carbon\Carbon::parse($session->created_at)->format('d/m/Y') }}</td> 
                    <td><a href="{{ route('dashboard.scores.show', $session->id) }}">Click to view</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>        
    </div>
    @if($data != 0)
        <div class="chartBorder mx-md-5 pt-3">
            <div class="row px-md-5">
                <div class="col-12">
                    <canvas id="chart" width="400" height="400"></canvas>
                </div>
            </div>    
        </div>
        <div class="row pl-5 mt-5 ml-md-4 pb-5">        
            @foreach($data['datasets'] as $key=>$value)            
            <div class="col-12 col-md-4 offset-md-1">
                <a id="emotionName{{$key}}" onclick="togglePlot({{ $key }})">
                    <div class="row">
                        <div class="col-1 px-0">
                            <div class="listButton" style="background-color: {{$value['borderColor']}};"></div>
                        </div>
                        <div class="col-11 mt-0">
                            <h3 class="mt-0" style="font-weight: 400; color: #4285f4;">{{ $value['label'] }}</h3>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach   
        </div>
        @endif
        <div class="row">
            <div class="col-4 offset-4 pb-5">
                <a href="{{ route('dashboard.patients.index') }}" class="btn btn-primary btn-block">
                    BACK
                </a>
            </div>
        </div>
@endsection
@section('scripts')
    <script>
        $(document).ready( function () {
            $('#table1').DataTable();
        });    
    </script>

<script>
    var canvas = document.getElementById('chart');
    var ctx = canvas.getContext("2d");
    const data = @json($data);    
    var hideData = Array(data.datasets.length).fill(false);
    var chart = new Chart(canvas, {
        type: 'line',
        data: {
            labels: data.allDates,
            datasets:  @json($data).datasets,
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            },
            maintainAspectRatio: false,
            legend: {
                display: false
            },
        }
    });

    canvas.onclick = function (evt) {
        var points = chart.getElementAtEvent(evt);     
        var datasetIndex = points[0]._datasetIndex;
        var itemIndex = points[0]._index;        

        $.ajax(
        {
            url : "{{ route('charts.get') }}",
            type: "GET",
            data :  {
                _method: 'GET',
                id: chart.data.datasets[datasetIndex].id[itemIndex],
                _token: '{{ csrf_token() }}'
            },
            success: function(response){                
                $('#showChartDataModal').on('show.bs.modal', function() {
                    $('#title').html(response.valueName);
                    $('#date').html(chart.data.labels[itemIndex]);
                    $('#note').html(response.note);   
                    $('#score').html(Math.round(response.score * 100) /100);            
                });
                    $('#showChartDataModal').modal('show');
                }
        });
    };

    function togglePlot(index) {
        if (hideData[index] === false) {
            hideData[index] = true;
            chart.data.datasets[index].data = [];
            $("#emotionName" + index).addClass("disabledLink");
        } else {
            hideData[index] = false;
            chart.data.datasets[index].data = [...data.datasets[index].data];
            $("#emotionName" + index).removeClass("disabledLink");
        }
        chart.update();
    }  

    
    </script>
@endsection
