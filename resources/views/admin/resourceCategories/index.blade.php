@extends('layouts.admin')
@section('content')
<h1 class="text-center pt-5">Resource Categories
<button class="btn btn-space btn-success" data-toggle="modal" data-target="#resourceModal" type="button">
  Add New Resources
</button>
</h1>
<div class="col-12">
  <table class="table table-striped table-hover table-fw-widget" id="table1">
    <thead>
      <tr>
          <th width="33%">Resource Name</th>
          <th width="33%">URL text</th>     
          <th width="33%">Action</th>     
      </tr>
    </thead>
    <tbody>
      @foreach ($allResourceCategories as $category)
      <tr>

        <td>{{ $category->name }}</td>
        <td>{{ $category->slug }}</td>       
        <td>
          <form-group>
            <button
            class="btn btn-primary md-trigger"
            type="button"
            data-name="{{ $category->name }}"           
            data-id="{{ $category->id }}"
            data-toggle="modal"
            data-target="#edit-resource">Edit</button>
            <button
            class="btn btn-danger md-trigger"
            type="button"
            data-id="{{ $category->id }}"
            data-toggle="modal"
            data-target="#delete-resource">Delete</button>
        </form-group>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<div class="modal fade" id="resourceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Add New Resource</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="{{ route('admin.resourceCategories.store')}}" method="POST">
            @csrf
              <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                        <label for="name"><h4>Link Text</h4></label>
                        <input class="form-control" type="text" name="name" id="name">
                    </div>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
        </div>
    </div>
</div>
{{-- edit Category modal --}}
<div class="modal fade" id="edit-resource" role="dialog">
  @include('includes.modals.editResourceCategoryModal')
</div>

{{-- delete Category modal --}}
<div class="modal fade" id="delete-resource" role="dialog">
  @include('includes.modals.deleteResourceCategoryModal')
</div>
@endsection



@section('scripts')
<script>
  $(document).ready( function () {
      $('#table1').DataTable();
  });
$('#edit-resource').on('show.bs.modal', function (event) {
  let button = $(event.relatedTarget);
  let modal = $(this);
  let resource_name = button.data('name'); 
  let id = button.data('id');
  modal.find('.modal-content #resource_name').val(resource_name);  
  const url = "{{ route('admin.resourceCategories.update', ':id') }}"
  $('#editForm').attr("action", url.replace(':id', id));
});
$('#delete-resource').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let modal = $(this);
    let id = button.data('id');
    const url = "{{ route('admin.resourceCategories.delete', ':id') }}"
    $('#deleteForm').attr("action", url.replace(':id', id));
  })
</script>

@endsection
