@extends('layouts.admin')

@section('content')
    <div class="main-content container-fluid">
        <h1>ADMIN PANEL</h1>
        @include('includes.admin.stats.topNumbers')
        @include('includes.admin.stats.graphs')
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //-initialize the javascript
            App.init();
            App.dashboard();

        });
    </script>
    @endsection
