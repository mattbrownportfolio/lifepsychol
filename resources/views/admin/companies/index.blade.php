@extends('layouts.admin')
@section('content')
    <h1 class="text-center pt-5">Companies
        <button class="btn btn-space btn-success" data-toggle="modal" data-target="#resourceModal" type="button">
            Add New Company
          </button>
    </h1>
    <div class="col-12">
        <table class="table table-striped table-hover table-fw-widget text-center" id="table1">
            <thead>
            <tr>
                <th width="20%">Company Name</th>
                <th width="10%">Short Name</th>
                <th width="15%">Contact Name</th>
                <th width="20%">Contact Email</th>
                <th width="15%">Maximum Number of Codes</th>
                <th width="20%">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($allCompanies as $company)
                <tr>
                    <td>{{ $company->name }}</td>
                    <td>{{ $company->short_name }}</td>
                    <td>{{ $company->contactname }}</td>
                    <td>{{ $company->contactemail }}</td>
                    <td>{{ $company->maximum_code_count }}</td>                    
                    <td>
                        <form-group>
                            <button
                                class="btn btn-primary md-trigger"
                                type="button"
                                data-id="{{ $company->id }}"
                                data-companyname="{{ $company->name }}"
                                data-shortname="{{ substr($company->short_name, 2) }}"
                                data-contactname="{{ $company->contactname }}"
                                data-contactemail="{{ $company->contactemail }}"
                                data-maximum_code_count="{{ $company->maximum_code_count }}"
                                data-toggle="modal"
                                data-target="#edit-company">Edit</button>
                            <button
                                class="btn btn-danger md-trigger"
                                type="button"
                                data-id="{{ $company->id }}"
                                data-toggle="modal"
                                data-target="#delete-company">Delete</button>
                        </form-group>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="resourceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" id="exampleModalLongTitle">Add New Company</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form action="{{ route('admin.companies.store')}}" method="POST">
                @csrf
                <input type="hidden" value="adminCompany" name="source" id="source">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="companyname"><h4>Company Name</h4></label>
                            <input class="form-control" type="text" name="companyname" id="companyname">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="shortname"><h4>Company Short Name</h4></label>                            
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">ST</div>
                                </div>
                                <input class="form-control" type="text" name="shortname" id="shortname">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="firstname"><h4>Contact First Name</h4></label>
                            <input class="form-control" type="text" name="firstname" id="firstname">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="surname"><h4>Contact Surname</h4></label>
                            <input class="form-control" type="text" name="surname" id="surname">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="contactemail"><h4>Contact Email</h4></label>
                            <input class="form-control" type="email" name="email" id="email">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="codeCount"><h4>Maximum Number of Codes</h4></label>
                            <input class="form-control" type="text" name="codeCount" id="codeCount">
                        </div>
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
            </div>
        </div>
    </div>

        <div class="modal fade" id="edit-company" role="dialog">
            @include('includes.modals.editCompanyModal')
        </div>
        <div class="modal fade" id="delete-company" role="dialog">
            @include('includes.modals.deleteModal')
        </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $('#table1').DataTable();
        });
        $('#edit-company').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let modal = $(this);
            let companyname = button.data('companyname');
            let code = button.data('code');
            let shortname = button.data('shortname');
            let contactname = button.data('contactname');
            let contactemail = button.data('contactemail');
            let maximum_code_count = button.data('maximum_code_count');
            let id = button.data('id');
            modal.find('.modal-content #companyname').val(companyname);
            modal.find('.modal-content #code').val(code);
            modal.find('.modal-content #shortname').val(shortname);
            modal.find('.modal-content #contactname').val(contactname);
            modal.find('.modal-content #contactemail').val(contactemail); 
            modal.find('.modal-content #maximum_code_count').val(maximum_code_count);                       
            const url = "{{ route('admin.companies.update', ':id') }}"
            $('#editForm').attr("action", url.replace(':id', id));
        });
        $('#delete-company').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let modal = $(this);
            let id = button.data('id');
            const url = "{{ route('admin.companies.delete', ':id') }}"
            $('#deleteForm').attr("action", url.replace(':id', id));
        })
    </script>

@endsection
