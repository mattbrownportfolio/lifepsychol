@extends('layouts.admin')

@section('content')
    <h1 class="text-center">{{ $professional->company->name }}'s Healthcare Professionals 
        <button class="btn btn-space btn-success" data-toggle="modal" data-target="#newProfModal" type="button">
        Add New
      </button></h1>  
    <div class="col-10 offset-1">
        <table class="table table-striped table-hover table-fw-widget" id="table1">
            <thead>
            <tr>
                <th>Name</th>  
                <th>Email</th>  
                <th>Code</th>
                <th>Codes Used</th> 
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($allProfessionals as $professional)
                <tr>
                    <td>{{ $professional->CompanyUser->firstname . ' ' . $professional->CompanyUser->surname }}</td>    
                    <td>{{ $professional->CompanyUser->email }}</td>
                    <td>{{ $professional->code }}</td>
                    <td>{{ $professional->used }}</td>
                    <td>
                        <form-group>
                            <button
                                class="btn btn-primary md-trigger"
                                type="button"
                                data-id="{{ $professional->user_id }}"
                                data-firstname="{{ $professional->CompanyUser->firstname }}"
                                data-surname="{{ $professional->CompanyUser->surname }}"
                                data-email="{{ $professional->CompanyUser->email }}"
                                data-toggle="modal"
                                data-target="#edit-prof">Edit</button>
                            <button
                                class="btn btn-danger md-trigger"
                                type="button"
                                data-id="{{ $professional->id }}"
                                data-toggle="modal"
                                data-target="#delete-prof">Delete</button>
                        </form-group>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="modal fade" id="newProfModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" id="exampleModalLongTitle">Add New Healthcare Professional</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.admin.store')}}" method="POST">
                    @csrf
                    <input type="hidden" value="company" name="source">
                    <input type="hidden" value="{{ $professional->code }}" name="prof_code">
                    <input type="hidden" value="{{ $professional->id }}" name="prof_id">
                    <input type="hidden" value="{{ $professional->company_id }}" name="prof_comp_id">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="firstname"><h4>First Name</h4></label>
                                <input class="form-control" type="text" name="firstname" id="firstname" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="surname"><h4>Surname</h4></label>
                                <input class="form-control" type="text" name="surname" id="surname" required>
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="email"><h4>Email</h4></label>
                                <input class="form-control" type="text" name="email" id="email" required>
                            </div>
                        </div>                        
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-prof" role="dialog">
        @include('includes.modals.editAdminProfModal')
    </div>
    <div class="modal fade" id="delete-prof" role="dialog">
        @include('includes.modals.deleteModal')
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
       $(document).ready( function () {
            $('#table1').DataTable();
        });

        $('#edit-prof').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let modal = $(this);
            let firstname = button.data('firstname');
            let surname = button.data('surname');  
            let email = button.data('email');           
            let id = button.data('id');
            modal.find('.modal-content #firstname').val(firstname);
            modal.find('.modal-content #surname').val(surname);    
            modal.find('.modal-content #email').val(email);                      
            const url = "{{ route('dashboard.admin.update', ':id') }}"
            $('#editForm').attr("action", url.replace(':id', id));
        });
        $('#delete-prof').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let modal = $(this);
            let id = button.data('id');
            const url = "{{ route('dashboard.admin.destroy', ':id') }}"
            $('#deleteForm').attr("action", url.replace(':id', id));
        })
    </script>
    @endsection
