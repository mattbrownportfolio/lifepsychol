@extends('layouts.admin')
@section('content')
@if (Session::has('message'))
    <div class="alert alert-info text-center"><h2 class="mt-0 py-1">{{ Session::get('message') }}</h2></div>
@elseif (Session::has('deleted'))
    <div class="alert alert-danger text-center"><h2 class="mt-0 py-1">{{ Session::get('deleted') }}</h2></div>
@endif
<h1 class="text-center pt-5">Emotions
<button class="btn btn-space btn-success" data-toggle="modal" data-target="#emotionModal" type="button">
  Add New Emotion
</button>
</h1>
<div class="col-8 offset-2">
  <table class="table table-striped table-hover table-fw-widget text-center" id="table1">
    <thead>
      <tr>            
          <th>Emotion</th>  
          <th>Added By</th>
          <th>Action</th>
      </tr>
    </thead>
    <tbody>    
      @foreach ($allEmotions as $emotion)    
      <tr>     
        <td>{{ $emotion->name }}</td>       
        <td>{{ $emotion->addedBy === null ? 'System': 'User'}}</td> 
        <td>          
          <form-group>
            <button 
            class="btn btn-primary md-trigger" 
            type="button"
            data-emotion="{{ $emotion->name }}"       
            data-toggle="modal"
            data-target="#edit-emotion">Edit</button>
            <button 
            class="btn btn-danger md-trigger"
            type="button"
            data-id="{{ $emotion->id }}"
            data-toggle="modal"
            data-target="#delete-emotion">Delete</button>
        </form-group>
        </td>   
      </tr>
      @endforeach  
    </tbody>
  </table>
</div>

<div class="modal fade" id="emotionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Add New Emotion</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="{{ route('admin.emotions.store')}}" method="POST">
            @csrf
              <div class="row">
                  <div class="col-12">
                    <div class="form-group">
                        <label for="emotion"><h4>Emotion</h4></label>
                        <input class="form-control" type="text" name="emotion" id="emotion">
                    </div>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
        </div>
    </div>
</div>
{{-- edit question modal --}}
<div class="modal fade" id="edit-emotion" role="dialog">
  @include('includes.modals.editEmotionModal')
</div>

{{-- delete question modal --}}
<div class="modal fade" id="delete-emotion" role="dialog">
  @include('includes.modals.deleteEmotionModal')
</div>
@endsection



@section('scripts')
<script>
  $(document).ready( function () {
      $('#table1').DataTable();
  });
$('#edit-emotion').on('show.bs.modal', function (event) {
  let button = $(event.relatedTarget);
  let modal = $(this);
  let emotion = button.data('emotion');
  let id = button.data('id');      
  modal.find('.modal-content #emotion').val(emotion);
  const url = "{{ route('admin.emotions.update', ':id') }}"
  $('#editForm').attr("action", url.replace(':id', id));
});
$('#delete-emotion').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let modal = $(this);
    let id = button.data('id');
    const url = "{{ route('admin.emotions.delete', ':id') }}"
    $('#deleteForm').attr("action", url.replace(':id', id));
  })
</script>

@endsection
