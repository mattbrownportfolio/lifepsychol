@extends('layouts.home', [
    'title' => 'Feedback'
])
@section('content')
<div class="container">
    <h1 class="text-center">Feedback</h1>
    <div class="row px-4 text-center">
        <div class="col-12 px-0">
            <div class="pt-4 mb-4 trackValueWording trackValueWordingSmall" style="color: #344EA9;">If you have any feedback you'd like to provide, whether it is good, bad or indifferent, we'd like to hear it. You may also wish to leave feedback regarding suggested improvements you'd like to see in the future. Please share your thoughts - we will read and consider all feedback received.</div>
        </div>
    </div>
<form action="{{ route('feedback.send') }}">
    <textarea name="feedback" id="" cols="30" rows="10" class="form-control noteInputBoxBorder pt-3"></textarea>
    <div class="row">
        <div class="col-12 col-md-6 pt-2">
            <button class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">Send feedback</button>
        </div>
        <div class="col-12 col-md-6 pt-2">
            <a href="{{ url('/home') }}" class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">Return to Dashboard</a>
        </div>
    </div>
</form>
<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Send Feedback</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                If you have any feedback you'd like to provide, whether it is good, bad or indifferent, we'd like to hear it. You may also wish to leave feedback regarding suggested improvements you'd like to see in the future. Please share your thoughts - we will read and consider all feedback received.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
