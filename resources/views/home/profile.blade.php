@extends('layouts.app', [
    'title' => 'Home'
])
@section('content')

<div class="container px-0">   
    <div class="row text-center pb-4">
        <div class="col-12">
            <h2>Hello {{ $user->firstname }}, welcome to your Profile</h2>       
        </div>
    </div>
    <div class="row text-center pb-4">
        <div class="col-12">
            @if (session('status'))
            <div class="alert alert-success text-center mb-0">
            <h3 class="my-0">{{ session('status') }}</h3>
            </div>   
            @elseif (session('error'))
            <div class="alert alert-danger text-center mb-0">
            <h3 class="my-0">{{ session('error') }}</h3>
            </div>          
            @endif       
        </div>
    </div>
    <div class="row mb-3 mx-1">
        <div class="profileBoxLeft col"> 
            <div class="row pt-3">
                <div class="col-12 col-md-2 offset-md-3 px-2">
                    <h3>Your details:</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 offset-md-3 col-2 pl-2 pr-0 profileText">
                    Name:
                </div>
                <div class="col-md-6 col-7 profileText">
                    {{ $user->firstname }} {{ $user->surname }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 offset-md-3 col-2 pl-2 pr-0 profileText d-none d-md-block">
                    Date Of Birth: 
                </div>
                <div class="col-md-2 offset-md-3 col-2 pl-2 pr-0 profileText d-block d-md-none">
                    DOB: 
                </div>
                <div class="col-md-6 col-7 profileText">
                    {{ \Carbon\Carbon::parse($user->dob)->format('d/m/Y') }}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 offset-md-3 col-2 pl-2 pr-0 profileText">
                    Email: 
                </div>
                <div class="col-md-6 col-7 profileText">
                    {{ $user->email }}
                </div>
            </div>   
            <div class="row py-3 text-center">
                <div class="col-12 col-md-6 offset-md-3">
                    <button 
                    class="btn btn-primary btn-lg md-trigger" 
                    type="button" 
                    data-toggle="modal"
                    data-target="#edit-profile-details"
                    data-id="{{ $user->id }}"
                    data-firstname="{{ $user->firstname }}"
                    data-surname="{{ $user->surname }}"
                    data-dob="{{ \Carbon\Carbon::parse($user->dob)->format('d/m/Y') }}"
                    data-email="{{ $user->email }}"                  
                    >
                        Edit Your Details
                    </button> 
                </div>
            </div>                            
        </div>        
    </div>    
    <div class="row mx-1">
        <div class="profileBoxRight col">
            <div class="row pt-3">
                <div class="col-12 col-md-6 offset-md-3 pr-0 pl-2">
                    <h3>Healthcare details:</h3>
                </div>                
            </div>
            <div class="row">
                <div class="col-5 col-md-3 offset-md-3 pr-0 pl-2 profileText">
                    Sharing Details:  
                </div>
                <div class="col-4 pr-0 pl-1 profileText">
                    {{ $user->consent == 0 ? 'No' : 'Yes' }} 
                </div>
            </div>
            <div class="row">
                <div class="col-5 col-md-3 offset-md-3 pr-0 pl-2 profileText d-none d-md-block">
                    Membership Number:
                </div>
                <div class="col-5 col-md-3 offset-md-3 pr-0 pl-2 profileText d-block d-md-none">
                    Membership No:
                </div>
                <div class="col-4 pr-0 pl-1 profileText">
                     {{ $user->patientcode }}
                </div>
            </div>
            <div class="row">
                <div class="col-5 col-md-3 offset-md-3 pr-0 pl-2 profileText">
                    Healthcare Professional: 
                </div>
                <div class="col-4 pr-0 pl-1 profileText">
                     {{ $professional == null ? 'No Professional' : $professional->firstname . ' ' . $professional->surname }}
                </div>
            </div>         
            <div class="row py-3 text-center">
                <div class="col-12 col-md-6 offset-md-3">
                    <button 
                    class="btn btn-primary btn-lg md-trigger" 
                    type="button" 
                    data-toggle="modal"
                    data-target="#edit-healthcare-details"
                    data-id="{{ $user->id }}"                   
                    data-patientcode="{{ $user->patientcode }}"
                    >
                        Edit Your Details
                    </button> 
                </div>
            </div>   
        </div>
    </div>
</div>
    {{-- edit profile modal --}}
   <div class="modal fade " id="edit-profile-details" role="dialog">
        @include('includes.modals.editProfileModal')
   </div>
   <div class="modal fade " id="edit-healthcare-details" role="dialog">
    @include('includes.modals.editHealthcareModal')
</div>
<div class="modal fade bd-example-modal-lg" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="exampleModalLongTitle">Welcome!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Welcome to your dashboard. From here you have the following options:
                <ul class="pt-3">
                    <li><strong>Track:</strong> Complete/update a Tracker and view a past Tracker</li>
                    <li><strong>Reminders:</strong> Set a reminder to update your Tracker</li>
                    <li><strong>Charts:</strong> Analysis of your scoring using different graphs/charts</li>
                    <li><strong>Resources:</strong> Access to a various web resources to support {{config('app.name')}}.</li>
                    <li><strong>Diary:</strong> Where you have recorded any personal notes made </li>
                    <li><strong>Feedback:</strong> Give any feedback/comments you wish to provide on using {{config('app.name')}}
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$('#edit-profile-details').on('show.bs.modal', function (event) {
  let button = $(event.relatedTarget);
  let modal = $(this);
  let firstname = button.data('firstname');
  let surname = button.data('surname');
  let dob = button.data('dob');
  let email = button.data('email');
  let patientcode = button.data('patientcode');
  let id = button.data('id');
  modal.find('.modal-body #firstname').val(firstname);
  modal.find('.modal-body #surname').val(surname);
  modal.find('.modal-body #dob').val(dob);
  modal.find('.modal-body #email').val(email);
  const url = "{{ route('profile.update', ':id') }}"
  $('#editForm').attr("action", url.replace(':id', id));
});

$('#edit-healthcare-details').on('show.bs.modal', function (event) {
  let button = $(event.relatedTarget);
  let modal = $(this);
  let id = button.data('id'); 
  const url = "{{ route('healthcare.update', ':id') }}"
  $('#editFormHC').attr("action", url.replace(':id', id));
});
</script>

@endsection
