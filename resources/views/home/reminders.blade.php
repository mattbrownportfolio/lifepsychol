<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Smart Tracker</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/img/smiley-face.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/img/smiley-face.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/img/smiley-face.png') }}">
    </head>
    <body>
        <div class="full-height">
            <div class="container">
                <div class="row px-4 px-md-5 pt-5">
                    <div class="col-12">
                        <div class="content">
                            <div class="title">
                                <h1 style="color: white;">Set a Reminder</h1>
                                <img class="img-fluid pb-5" src="{{ asset('img/living_smart-icons-01.png') }}" alt="" width="200px">
                            </div>
                            <p style="color: white; font-size: 20px ">To receive a reminder to update your {{config('app.name')}}, select below. Once completed, click “Go to Dashboard” to exit</p>
                            {{-- <p style="color: white; font-size: 20px ">Your next reminder is in {{ $days + 1 }} days</p> --}}
                            <form action="{{ route('reminders.update') }}" method="post" class="pb-5">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <select class="form-control addEmotion" name="numberOfDays" onchange="this.form.submit()">
                                            <option selected>{{ $days > 1 ? $days + 1 .' days': $days + 1 . ' day' }} </option>
                                            <option value="1">1 day</option>
                                            <option value="7">7 days</option>
                                            <option value="14">14 days</option>
                                            <option value="28">28 days</option>
                                            <option value="84">3 months</option>
                                            <option value="168">6 months</option>
                                        </select>
                                    </div>
                                </div>
                                @if(\Session::has('success'))
                                    <div class="alert alert-success my-3 py-0">
                                        {!! \Session::get('success') !!}
                                    </div>
                                @endif
                                <div class="row pt-4">
                                    <div class="col-12">
                                        <a href="{{ route('home') }}" class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">Go to Dashboard</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
