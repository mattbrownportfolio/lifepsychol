@extends('layouts.home', [
    'title' => 'Overall Summary'
])
@section('content')
<div class="container pl-0">
    <div class="row pb-2">
        <div class="col-12 summaryText text-center trackValueWording pb-3">My Tracker Overview</div>
        <p class="summaryInstruction">Here are your scores based on how you rated your values today. If you wish to change the order of the below values to show your most important value(s) at the top you can do so by simply clicking and dragging them. To view more information on the emotions you chose, and the notes you left, please click on the Value name of your choice</p>
    </div>    
    <div class="row">
        <div class="col-6 pl-2 pl-md-0 summaryText">Negative</div>    
        <div class="col-6 pr-0 text-right summaryText">Positive</div>
    </div>
    
        <div class="row px-3 px-md-0 pt-2">                
            <div class="col-12 pl-0">                    
                <div class="row">
                    <div class="col-10">
                        <h4><a href="#">Overall</a></h4>
                    </div>                              
                </div>    
                @php           
                    $valueCount = $sessionValues->count();      
                    $marker = (($sessionScore / $valueCount) * 100);
                @endphp                         
                <div class="row pl-3 d-none d-md-block">      

                    <div class="col-12 sliderBarLarge pr-0" style="padding-left: {{ $marker > 95 ? $marker-2 : $marker }}%">
                        <div class="slider shadow">
                            &nbsp
                        </div>
                    </div>
                </div>
                <div class="row pl-3 d-block d-md-none">
                    <div class="col-12 sliderBarLarge pr-0" style="padding-left: {{ $marker > 95 ? $marker-5 : $marker }}%">
                        <div class="slider shadow">
                            &nbsp
                        </div>
                    </div>
                </div>                        
            </div>                
        </div> 
        <div id="values">
        @foreach ($sessionValues as $value)
        @php
            $count = $value->emotions->count(); 
            $total = $count * 2; 
            $marker = ((($value->score * $count) + $count) / $total) * 100;
        @endphp      
            <div class="row px-3 px-md-0 pt-2" id="value_{{ $value->id }}">                
                <div class="col-12 pl-0">                    
                    <div class="row">
                        <div class="col-10">
                            <h4><a href="{{ route('summarydetails', $value->id )}}">{{ $value->value->name }}</a></h4>
                        </div>                              
                    </div>                             
                    <div class="row pl-3 d-none d-md-block">
                        <div class="col-12 sliderBarLarge pr-0" style="padding-left: {{ $marker > 95 ? $marker-2 : $marker }}%">
                            <div class="slider shadow">
                                &nbsp
                            </div>
                        </div>
                    </div>
                    <div class="row pl-3 d-block d-md-none">
                        <div class="col-12 sliderBarLarge pr-0" style="padding-left: {{ $marker > 95 ? $marker-5 : $marker }}%">
                            <div class="slider shadow">
                                &nbsp
                            </div>
                        </div>
                    </div>                        
                </div>                
            </div>       
        @endforeach
    </div>
    <div class="row px-2 px-md-4 pt-4">
        <div class="col-8 col-md-4 pt-3 pr-0">
            <h4>My Summary Score:</h4>
        </div>
        <div class="col-4 col-md-2 pl-0">
            @if ($sessionScore < 0)
            <div class="scoreCircle pb-4 text-center" style="background-color: lightgrey; color: #344EA9">{{ round($sessionScore, 1) }}</div>
            @else
            <div class="scoreCircle pb-4 text-center">{{ round($sessionScore, 1) }}</div>
            @endif
        </div>
        @if($sessionCount > 0)
        <div class="col-6 col-md-3 pt-2">
            <a href="{{ url('/compare') }}" class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">Compare</a>
        </div>
        @else
        <div class="col-6 col-md-3 pt-2">
    
        </div>
        @endif
        <div class="col-6 col-md-3 pt-2">
            <a href="{{ url('/home') }}" class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">Home</a>
        </div>
    </div>
    
    <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">My Tracker</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <p>Here you are presented with a Tracker overview and how you generally feel about key areas that make up your quality of life. You can click on each ‘Value’ that you have scored. This will open a screen to view the emotion words that you previously selected which will allow you to view more details on the emotions you chose, and the notes you left.</p>
                <p>Click on ”Compare” to be able to compare two different Summary Scores.</p>
                <p>My Summary Score: This is a numerical value calculated by the {{config('app.name')}}. This score is designed to support discussion with any healthcare professional who may support you, as it is to help provide trend over time.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
</div>

{{-- show modal --}}
<div class="modal fade" id="show-score" role="dialog">
    @include('includes.modals.showScoresModal')
  </div>

@endsection
@section('script')
<script>   
    $( function() {
        $( "#values" ).sortable({
            axis: "y", 
                    
            update : function (event, ui) {
                var data = $(this).sortable('serialize');
                updateValues(data);
            },
            start: function( event, ui ) {
            
            }
            });
        $( "#values" ).disableSelection();
  } );

  function updateValues(data) {
      $.ajax(
        {
        url : '{{ route('overallsummary.update') }}',
        type: "POST",
        data : data + "&_token={{ csrf_token() }}&_method=patch",
        success:function(data, textStatus, jqXHR)
        {
            $.gritter.add({
            title: "Order updated",
            // text: "Any changes you make will not be replicated on the live site. You can use this site for testing.",
            class_name: "color success"
            })
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            alert('Could not re-order items. Please try again.');
        }
        });
    }

    $('#show-score').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let modal = $(this);
    let score = button.data('score');
    let count = button.data('count');
    let title = button.data('title');
    $('#count').html(count);
    $('#score').html(score);   
    $('#title').html(title);
    });
</script>
@endsection
