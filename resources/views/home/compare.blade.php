<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/img/smiley-face.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/img/smiley-face.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/img/smiley-face.png') }}">
    </head>
    <body>
        <div class="full-height pb-5">
            <div class="container">
                <div class="row px-2 px-md-5 pt-5">
                    <div class="col-12">
                        <div class="content">
                            <div class="title m-b-md">
                                <img class="img-fluid pb-3" src="{{ asset('img/living_smart-icons-03.png') }}" alt="" width="250px">
                            </div>
                            <h4 style="color: white;">Here you can compare the tracker you completed today with a previously completed tracker. Select a date for a previously completed tracker by selecting the ”Session Date”. This may be useful when assessing changes.</h4>
                        </div>
                    </div>
                </div>
                <div class="row text-center text-white">
                    <div class="col-12 pb-3">
                        <h1>Today</h1>
                    </div>
                </div>
                <div class="row px-2 px-md-5">
                    <div class="col-12 summaryPills">
                        <div class="row">
                            <div class="col-9 col-lg-11">
                                <p class="scoreLargeFont pt-2">Overall Score:</p>
                            </div>
                            <div class="col-2 col-lg-1 text-center">
                                @if ($sessionScore < 0)
                                <p class="scoreLargeFont smallCircle text-center mx-0"  style="background-color: lightgrey; color: blue">{{ round($sessionScore, 1) }}</p>
                                @else
                                <p class="scoreLargeFont smallCircle text-center mx-0">{{ round($sessionScore, 1) }}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row text-white">
                    <div class="col-12 py-3 text-center">
                        <h1>Choose Session Date:</h1>
                        <div class="row">
                            <div class="col-8 offset-2">
                                <form action="{{ route('compareresults') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="currentSessionId" value="{{ $sessionId }}" id="hiddenIdField">
                                    <select class="form-control" id="sessionDates" name="compareDates">
                                        @foreach ($allSessions as $session)
                                            <option value="{{ $session->id }}" data-score="{{ $session->totalScore() }}" class="text-center">{{ $session->created_at->format('d M Y') }}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row px-2 px-md-5">
                    <div class="col-12 summaryPills">
                        <div class="row">
                            <div class="col-9 col-lg-11">
                                <p class="scoreLargeFont pt-2">Overall Score:</p>
                            </div>
                            <div class="col-2 col-lg-1 text-center">
                                <p class="scoreLargeFont smallCircle text-center mx-0" id="showScore"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row px-2 px-md-5 pt-4">
                    <div class="col-6">
                        <button class="btn btn-primary btn-block loginButton" role="button" type="submit" style="background-color: #344EA9; border: none;">See Detailed Summaries</button>
                    </div>
                    <div class="col-6">
                        <a href="{{ url('/home') }}" class="btn btn-primary btn-block loginButton" role="button" type="submit" style="background-color: #344EA9; border: none;">Home</a>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </body>
    <script>
        $(document).ready(function(){
            $("#sessionDates").change(function(){
                let score = Math.floor($(this).find('option:selected').attr('data-score'));
                $('#showScore').text((score > 0 ? '+' : '') + score);
            });
        });
    </script>
</html>
