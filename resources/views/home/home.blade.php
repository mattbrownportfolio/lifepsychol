@extends('layouts.home', [
    'title' => 'Home'
])
@section('content')

<div class="mainMenuIcons">   
    <div class="row text-center pb-4">
        <div class="col-12">           
            {{-- <img class="img-fluid px-4 pb-4" src="{{ asset('img/mainlogo.svg') }}" alt="" width="350px"> --}}
            <h3 class="pt-2 pb-3">Hello {{ Auth::user()->firstname }}, welcome to your dashboard.</h3>
            @if (count($lastSession) > 0 && Auth::user()->role_id == 2)
            <h5 class="pb-3">You last completed your {{config('app.name')}} on {{ $lastSession->where('complete', 1)->last()->created_at->format('d M Y') }}.</h5>
            @endif
        </div>
    </div>
    <div class="row text-center">
        <div class="col-6 col-md-4 px-0 pb-3 pb-md-5">
            <a href="{{ url('/reminders') }}">
                <img class="img-fluid homeScreenIcons" src="{{ asset('img/living_smart-icons-01.png') }}" alt="">
                <h3 class="pt-2 homePageHeaderText">Reminders</h3>
            </a>
        </div>
        <div class="col-6 col-md-4 px-0 pb-3 pb-md-5">
            @if(Auth::user()->role_id == 2)
                @if(count($lastSession) > 0 && $lastSession->last()->complete === 0)
                <a href="{{ url('track')}}" data-toggle="modal" data-target="#completeModal">
                @else
                <a href="{{ url('track')}}">
                @endif
            @endif
                <img class="img-fluid homeScreenIcons" src="{{ asset('img/living_smart-icons-09.png') }}" alt="">
                <h3 class="pt-2 homePageHeaderText">Track</h3>
            </a>
        </div>
        <div class="col-6 col-md-4 px-0 pb-3 pb-md-5">
            <a href="{{ route('charts') }}">
                <img class="img-fluid homeScreenIcons" src="{{ asset('img/living_smart-icons-03.png') }}" alt="">
                <h3 class="pt-2 homePageHeaderText">Trends</h3>
            </a>
        </div>
        <div class="col-6 col-md-4 px-0 pb-3 pb-md-5">
            <a href="{{ url('resources/anger-and-frustration') }}">
                <img class="img-fluid homeScreenIcons" src="{{ asset('img/living_smart-icons-04.png') }}" alt="">
                <h3 class="pt-2 homePageHeaderText">Resources</h3>
            </a>
        </div>
        <div class="col-6 col-md-4 px-0 pb-3 pb-md-5">
            <a href="{{ route('diary') }}">
                <img class="img-fluid homeScreenIcons" src="{{ asset('img/living_smart-icons-05.png') }}" alt="">
                <h3 class="pt-2 homePageHeaderText">Diary</h3>
            </a>
        </div>
        <div class="col-6 col-md-4 px-0 pb-3 pb-md-5">
            <a href="{{ route('feedback') }}">
                <img class="img-fluid homeScreenIcons" src="{{ asset('img/living_smart-icons-06.png') }}" alt="">
                <h3 class="pt-2 homePageHeaderText">Feedback</h3>
            </a>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="exampleModalLongTitle">Welcome!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Welcome to your dashboard. From here you have the following options:
                <ul class="pt-3">
                    <li><strong>Track:</strong> Complete/update a Tracker and view a past Tracker</li>
                    <li><strong>Reminders:</strong> Set a reminder to update your Tracker</li>
                    <li><strong>Charts:</strong> Analysis of your scoring using different graphs/charts</li>
                    <li><strong>Resources:</strong> Access to a various web resources to support {{config('app.name')}}.</li>
                    <li><strong>Diary:</strong> Where you have recorded any personal notes made </li>
                    <li><strong>Feedback:</strong> Give any feedback/comments you wish to provide on using {{config('app.name')}}
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="completeModal">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Info</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Would you like to carry on from the last session, or start a new one?</p>          
        </div>
        <div class="modal-footer">
        @if(count($lastSession) > 0 && Auth::user()->role_id == 2)
          <a href="{{ url('track', ['stage' => $lastSession->last()->stage+1])}}" type="button" class="btn btn-primary">Complete Last Session</a>
          @endif
          <a href="{{ url('track')}}" type="button" class="btn btn-success">New Session</a>
          <div class="d-none d-md-block">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection