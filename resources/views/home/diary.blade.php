@extends('layouts.home', [
    'title' => 'Diary'
])
@section('content')
<div class="container">
  <div class="row text-center px-5 mt-2">
    <div class="col-12">
        <h1>Diary</h1>
    </div>
</div>
<div class="row px-4 text-center">
    <div class="col-12 px-0">
        <div class="pt-4 mb-4 trackValueWording trackValueWordingSmall" style="color: #344EA9;">To view the notes, click the entry in the diary below</div>
    </div>
</div>
<div class="be-content">
    <div class="main-content container-fluid">
      <div class="row full-calendar">
        <div class="col-lg-12 px-0">
          <div class="card card-fullcalendar">
            <div class="card-body">
              <div id="calendar"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <a href="{{ url('/home') }}" class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">Dashboard</a>

  <div class="modal fade" id="showDiaryEntryModal" role="dialog">
    @include('includes.modals.showDiaryEntryModal')
  </div>
  <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Diary</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           Here you can choose a certain date you completed a session on to see your notes from that session for each value.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="{{ asset('lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('lib/perfect-scrollbar/js/perfect-scrollbar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('lib/jquery.fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //-initialize the javascript
        App.init();
        App.pageCalendar();
    });
    var events = @json($events);
    $('#calendar').fullCalendar({
      height: 500,
    events,
    eventClick: function(calEvent, jsEvent, view) {
      $('#showDiaryEntryModal').on('show.bs.modal', function() {
        $('#title').html(calEvent.title);
        $('#body').html(calEvent.note);
      });
        $('#showDiaryEntryModal').modal('show');
    }
    });
  </script>
@endsection
