@extends('layouts.resources')

@section('content')
<a href="#" class="js-toggle-fixed-sidebar text-left pt-3 pl-2"><span class="material-icons md-48">list</span></a>   
<div class="text-center">
    <img src="{{ asset('img/mainlogo.svg') }}" alt="" style="width: 300px;"> 
    {{-- <h1>Resources</h1> --}}
</div>
<h1 class="text-center py-3"> Resources | {{$categoryName}} </h1>
<div class="row text-center mx-2">
    @foreach ($allResources as $resource)
    <div class="mx-2 mx-md-3 pb-4 px-1 px-md-3 mb-3 pt-3 resourceSquare">
        <h3><a href="{{ $resource->link_url }}" style="color: #344EA9" class="resourceLink" target="_blank" data-id="{{ $resource->id}}">{{ $resource->link_text }}</a></h3>
        <div class="d-none d-md-block">
            {{ $resource->description }}
        </div>
    </div>
    @endforeach
</div>
<div class="row py-5 px-3">
    <div class="col-12 text-center">
        <a href="{{ url('/home') }}" class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">Return to Dashboard</a>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('.resourceLink').click(function(){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/update_resource_click",
                type:"POST",
                data: {
                    resource_id:   $(this).attr('data-id')
                },
                success: function (data) {
                    console.log(data);
                },
                error: function (request, status, error) {
                    console.log('code: '+request.status+"\n"+'message: '+request.responseText+"\n"+'error: '+error);
                }
            });
        });
    </script>
@endsection
