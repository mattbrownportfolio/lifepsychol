@extends('layouts.home')
@section('content')
<div class="container">
  <div class="row text-center px-2 px-md-5">
    <div class="col-12">
        <div class="pt-3 trackValueWording">How are you feeling about your...</div>
        <div class="py-2 trackValueWording" style="font-weight: 800">{{ $allValuesName }} <br> ({{ $index}} of {{ $allValuesCount}})</div>      
    </div>
  </div>
  <div class="row px-2 px-md-5">
      <div class="col-12">
          <div class="pb-3 trackValueWording trackValueWordingSmall pt-2">If you wish to explain your selection, you can document your reasons here</div>
      </div>
  </div>
  <form action="{{ route('storeNotes') }}" method="POST">
    <div class="row px-2 px-md-5 inputBoxWrapper">
      <div class="col-12">
        @csrf
        <input type="hidden" name="pageNumber" value="{{ $index }}">
        <input type="hidden" name="valueId" value="{{ $allValuesId }}">
        <input type="hidden" name="sessionValueId" value="{{ $session_value_id }}">
        <textarea name="note" id="" cols="30" rows="12" class="form-control noteInputBox pt-3 noteInputBoxBorder" placeholder=""></textarea>
      </div>
    </div>
    <div class="row pb-3 px-5 pt-4 fixed-row-bottom">      
      <div class="col-6">
        @if(!$isLast)
          <a href="{{ route('trackemotions', $index + 1) }}" class="btn btn-outline-primary btn-block loginButtonOutline" role="button">Skip</a>
        @else
          <button href="{{ route('home') }}" class="btn btn-outline-primary btn-block loginButtonOutline" role="button">Skip</button>
        @endif
      </div>
      <div class="col-6">
        <button class="btn btn-primary btn-block loginButton" type="submit" role="button">Submit</button>
      </div>
    </div>
  </form>
<div class="modal fade" id="helpModalHome" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Adding Notes</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           If you wish to add notes to aid you later on with remembering your reasons for scoring, you can add them here.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
