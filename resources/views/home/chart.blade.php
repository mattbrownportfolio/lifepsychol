@extends('layouts.home')
@section('content')
<div class="container">
    <div class="row px-md-4">
        <div class="col-12 text-center">
            <h1>Analysis</h1>
            <h4>Here the {{config('app.name')}} shows how your scoring is changing over time. We will be developing this section in the coming months. At present it helps to offer some insight and track how you may be feeling about certain ‘life’ values and aid any communication you may wish to have with a health professional</h4>
            @if($data == 0)
            <div class="text-center py-5"><h4>You have not yet recorded any scores</h4></div>
            @endif
        </div>
    </div>
    @if($data != 0)
    <div class="chartBorder mx-md-5 pt-3">
        <div class="row px-md-5">
            <div class="col-12">
                <canvas id="chart" width="400" height="400"></canvas>
            </div>
        </div>    
    </div>
    <div class="row pl-5 mt-5 ml-md-4">        
        @foreach($data['datasets'] as $key=>$value)
        <div class="col-12 col-md-6">
            <a href="#" id="emotionName{{$key}}" onclick="togglePlot({{ $key }})"><div class="row">
                <div class="col-1 px-0">
                    <div class="listButton" style="background-color: {{$value['borderColor']}};"></div>
                </div>
                <div class="col-11">
                    <h3>{{ $value['label'] }}</h3>
                </div>
            </div>
            </a>
        </div>
        @endforeach   
    </div>
    @endif
    <div class="row px-md-4 pt-4">
        <div class="col-12 text-center">
            <a href="{{ url('/home') }}" class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">Back to Dashboard</a>
        </div>
    </div>
    <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Charts</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               Here you can see how you scored each value each session. If you wish, you can choose to hide or show certain values by clicking them below the graph.
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
</div>

<div class="modal fade" id="showChartDataModal" role="dialog">
    @include('includes.modals.showChartDataModal')
</div>
@endsection
@section('script')
<script>
    var canvas = document.getElementById('chart');
    var ctx = canvas.getContext("2d");
    const data = @json($data); 
    console.log(data)   ;
    var hideData = Array(data.datasets.length).fill(false);
    var chart = new Chart(canvas, {
        type: 'line',
        data: {
            labels: data.allDates,
            datasets:  @json($data).datasets,
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    }
                }]
            },
            maintainAspectRatio: false,
            legend: {
                display: false
            },
        }
    });

    canvas.onclick = function (evt) {
        var points = chart.getElementAtEvent(evt);     
        var datasetIndex = points[0]._datasetIndex;
        var itemIndex = points[0]._index;        

        $.ajax(
        {
            url : "{{ route('charts.get') }}",
            type: "GET",
            data :  {
                _method: 'GET',
                id: chart.data.datasets[datasetIndex].id[itemIndex],
                _token: '{{ csrf_token() }}'
            },
            success: function(response){                
                $('#showChartDataModal').on('show.bs.modal', function() {
                    $('#title').html(response.valueName);
                    $('#date').html(chart.data.labels[itemIndex]);
                    $('#note').html(response.note);   
                    $('#score').html(Math.round(response.score * 100) /100);            
                });
                    $('#showChartDataModal').modal('show');
                }
        });
    };

    function togglePlot(index) {
        if (hideData[index] === false) {
            hideData[index] = true;
            chart.data.datasets[index].data = [];
            $("#emotionName" + index).addClass("disabledLink");
        } else {
            hideData[index] = false;
            chart.data.datasets[index].data = [...data.datasets[index].data];
            $("#emotionName" + index).removeClass("disabledLink");
        }
        chart.update();
    }  

    
    </script>
@endsection
