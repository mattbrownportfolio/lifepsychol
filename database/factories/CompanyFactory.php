<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
            'name' => $faker->company,
            'short_name' => $faker->word,
            'maximum_code_count' => $faker->numberBetween($min = 50, $max = 2000),
            'used' => 0,
            'contactname' => $faker->name,
            'contactemail' => $faker->email,
    ];
});
