<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(User::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstname,
        'surname' => $faker->lastname,
        'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'gender' => $faker->numberBetween($min = 0, $max = 2),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'trauma' => 0,
        'role_id' => 3,
        'consent' => 0,
        'professional_id' => 0,
        'location' => 'London'
    ];
});
